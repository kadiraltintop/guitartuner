//
//  PremiumViewController.swift
//  Guitar Tuner
//
//  Created by Dev on 5.12.2020.
//

import UIKit
import Lottie
import Purchases
import JGProgressHUD
import SafariServices
import FirebaseAnalytics

class PremiumViewController: UIViewController {
    
    @IBOutlet weak var lbl3: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var restoreButton: UIButton!
    @IBOutlet weak var privacyButton: UIButton!
    @IBOutlet weak var termsButton: UIButton!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var lottieView: AnimationView!
    
    var annualProduct : SKProduct!
    var weeklyProduct : SKProduct!
    var monthlyProduct : SKProduct!
    var selectedProduct : SKProduct!
    
    var loader = JGProgressHUD()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        descriptionLbl.text = "try_premium".localized
        startButton.setTitle("start_now".localized, for: .normal)
        termsButton.setTitle("terms".localized, for: .normal)
        privacyButton.setTitle("privacy".localized, for: .normal)
        restoreButton.setTitle("restore".localized, for: .normal)
        lbl1.text = "ad_free".localized
        lbl2.text = "unlimited_library".localized
        lbl3.text = "unlimited_metronome".localized
        
        //startButton.layer.cornerRadius = 23
       // startButton.layer.shadowColor = #colorLiteral(red: 2.151550916e-06, green: 0.6969773173, blue: 0.371265173, alpha: 1)
        //startButton.layer.shadowOpacity = 1
       // startButton.layer.shadowOffset = .zero
       // startButton.layer.shadowRadius = 10
        
        self.lottieView.contentMode = .scaleToFill
        self.lottieView.animation = Animation.named("premium")
        self.lottieView.loopMode = .loop
        self.lottieView.play()

        getPrice()
        
    }
    
    func getPrice(){
        Purchases.shared.offerings { (offerings, error) in
            
            if let offerings = offerings{
                print(offerings.current)
            }
          
            
            if let e = error{
                print(error?.localizedDescription)
                return
            }
            if let annual = offerings!.current?.annual?.product{
                self.annualProduct = annual
              //  self.descLbl3.text = self.descLbl3.text!.replacingOccurrences(of: "#", with: annual.localizedPrice)
            }
            if let weekly = offerings!.current?.weekly?.product{
                self.weeklyProduct = weekly
                self.descriptionLbl.text = self.descriptionLbl.text!.replacingOccurrences(of: "#", with: weekly.localizedPrice)
            }
            if let monthly = offerings!.current?.monthly?.product{
                self.monthlyProduct = monthly
               // self.descLbl2.text = self.descLbl2.text!.replacingOccurrences(of: "#", with: monthly.localizedPrice)
            }
            
            self.selectedProduct = self.weeklyProduct
        }
    }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func startButtonTapped(_ sender: Any) {
        self.loader.show(in: self.view)
        
        Purchases.shared.purchaseProduct(selectedProduct) { (transaction, info, error, cancelled) in
            self.loader.dismiss()
            
            if cancelled{
                print("User cancelled purchase")
                return
            }
            if let err = error as NSError?{
                print(err)
            }else if info!.activeSubscriptions.first?.contains("gt_499_1w") ?? false{
                
              
                Analytics.logEvent("weekly_trial_started", parameters: [:])
                UserDefaults.standard.set(true, forKey: "isPremium")
                NotificationCenter.default.post(name: .premiumNotification, object: nil)
                self.dismiss(animated: true)
                
            }
        }
    }
    
    @IBAction func termsButton(_ sender: Any) {
        let SafariVC = SFSafariViewController(url: URL(string: "http://www.yesilsoft.net/appsupport")!)
        self.present(SafariVC, animated: true, completion: nil)
    }
    
    @IBAction func privacyButton(_ sender: Any) {
        let SafariVC = SFSafariViewController(url: URL(string: "http://www.yesilsoft.net/privacypolicy")!)
        self.present(SafariVC, animated: true, completion: nil)
    }
    
    @IBAction func restoreButton(_ sender: Any) {
        //Eger kullanici premiumsa ama app silindiyse restore islemiyle premiuma tekrar donebilir. Apple tarafindan zorunlu tutulan bir ozellik.
        
        self.loader.show(in: self.view)
        Purchases.shared.restoreTransactions { (purchaserInfo, error) in
            self.loader.dismiss()
            if let error = error{
                print("Restore Failed: \(error.localizedDescription)")
                self.showError("Restore Failed", message: error.localizedDescription)
            }else{
                print("Restore Success: \(String(describing: purchaserInfo?.entitlements.active))")
                    //active kontrolu onemli
                if(purchaserInfo?.entitlements.active.count ?? 0 > 0) {
                    UserDefaults.standard.set(true, forKey: "isPremium")
                    NotificationCenter.default.post(name: .premiumNotification, object: nil)
                    self.dismiss(animated: true, completion: nil)
                }else{
                    self.showError("Error", message: "Nothing to restore")
                }
            }
        }
    }
}
