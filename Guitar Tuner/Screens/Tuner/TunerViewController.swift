//
//  TunerViewController.swift
//  Guitar Tuner
//
//  Created by Dev on 29.11.2020.
//

import UIKit
import AudioKit
import AVFoundation
import GoogleMobileAds

class TunerViewController: UIViewController, TunerDelegate {
    
    
    
    
   //mixer denemeleri
    let path1 = Bundle.main.path(forResource: "do.wav", ofType:nil)!
    var file1 : AVAudioFile?
    let path2 = Bundle.main.path(forResource: "sol.wav", ofType:nil)!
    var file2 : AVAudioFile?
    let path3 = Bundle.main.path(forResource: "si.wav", ofType:nil)!
    var file3 : AVAudioFile?
    let path4 = Bundle.main.path(forResource: "mi.wav", ofType:nil)!
    var file4 : AVAudioFile?
    
    
    var player1 : AudioPlayer?
    var player2 : AudioPlayer?
    var player3 : AudioPlayer?
    var player4 : AudioPlayer?
    
    var bombSoundEffect1 : AVAudioPlayer?
    var bombSoundEffect2 : AVAudioPlayer?
    var bombSoundEffect3 : AVAudioPlayer?
    var bombSoundEffect4 : AVAudioPlayer?
    
    var node1 = Node(avAudioNode: AVAudioNode())
    
    let engine = AudioEngine()
    var mixer = Mixer()



    @IBOutlet weak var topInstrumentBtn: UIButton!
    @IBOutlet weak var autoTunerLbl: UILabel!
    @IBOutlet weak var autoTunerSwitch: UISwitch!
    var test : AVAudioPlayer!
    var frequencyView = UIImageView()
    var hzLbl = UILabel()
    var noteLbl = UILabel()
    var instrumentImg = UIImageView()
    var frequencyBoardImg = UIImageView()
    var bottomView = UIView()
    var instrumentView = UIView()
    var guitarBtn1 = UIButton()
    var guitarBtn2 = UIButton()
    var guitarBtn3 = UIButton()
    var guitarBtn4 = UIButton()
    var guitarBtn5 = UIButton()
    var guitarBtn6 = UIButton()
    var bassBtn1 = UIButton()
    var bassBtn2 = UIButton()
    var bassBtn3 = UIButton()
    var bassBtn4 = UIButton()
    var ukuleleBtn1 = UIButton()
    var ukuleleBtn2 = UIButton()
    var ukuleleBtn3 = UIButton()
    var ukuleleBtn4 = UIButton()
    var helperLbl = UILabel()
    var isPremium = false
    var interstitial : GADInterstitial!
    
    
    
    var selectedString = 1
    var targetFrequency : Float?
    
    
    
    var guitarChordStruct = [chordStruct(note: "E", frequency: 82.4), chordStruct(note: "A", frequency: 110.0), chordStruct(note: "D", frequency: 146.8), chordStruct(note: "G", frequency: 196.0), chordStruct(note: "B", frequency: 246.9), chordStruct(note: "E", frequency: 329.6),]
    var bassChordStruct = [chordStruct(note: "E", frequency: 41.2), chordStruct(note: "A", frequency: 55.0), chordStruct(note: "D", frequency: 73.4), chordStruct(note: "G", frequency: 98.0)]
    
    var ukuleleChordStruct = [chordStruct(note: "G", frequency: 392.0), chordStruct(note: "C", frequency: 261.6), chordStruct(note: "E", frequency: 329.6), chordStruct(note: "A", frequency: 440.0)]
    var guitarNoteArray = [notePitchStruct(note: "E", pitch: 2), notePitchStruct(note: "A", pitch: 2), notePitchStruct(note: "D", pitch: 3), notePitchStruct(note: "G", pitch: 3), notePitchStruct(note: "B", pitch: 3), notePitchStruct(note: "E", pitch: 4)]
    var ukuleleNoteArray = [notePitchStruct(note: "G", pitch: 4), notePitchStruct(note: "C", pitch: 4), notePitchStruct(note: "A", pitch: 4), notePitchStruct(note: "E", pitch: 4)]
    var bassNoteArray = [notePitchStruct(note: "E", pitch: 1), notePitchStruct(note: "A", pitch: 1), notePitchStruct(note: "D", pitch: 2), notePitchStruct(note: "G", pitch: 2)]
    
    // 0-guitar, 1-ukulele, 2-bass
    var selectedInstrument = 0
    var autoTuner = true
    var selectedNoteFrequency : Float = 440.0

    let tuner = TMTuner()
    var tolerance = 7.0
    var modelName = UIDevice.modelName
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //mixer denemeleri
        let url1 = URL(fileURLWithPath: path1 )
        let url2 = URL(fileURLWithPath: path2 )
        let url3 = URL(fileURLWithPath: path3 )
        let url4 = URL(fileURLWithPath: path4 )
        
        do {
            bombSoundEffect1 = try AVAudioPlayer(contentsOf: url1)
            file1 = try AVAudioFile(forReading: url1)
        } catch {
            // couldn't load file :(
        }
        do {
            bombSoundEffect2 = try AVAudioPlayer(contentsOf: url2)
            file2 = try AVAudioFile(forReading: url2)
        } catch {
            // couldn't load file :(
        }
        do {
            bombSoundEffect3 = try AVAudioPlayer(contentsOf: url3)
            file3 = try AVAudioFile(forReading: url3)
        } catch {
            // couldn't load file :(
        }
        do {
            bombSoundEffect4 = try AVAudioPlayer(contentsOf: url4)
            file4 = try AVAudioFile(forReading: url4)
        } catch {
            // couldn't load file :(
        }
        
        bombSoundEffect2?.prepareToPlay()
        bombSoundEffect1?.prepareToPlay()
        bombSoundEffect3?.prepareToPlay()
        bombSoundEffect4?.prepareToPlay()
        
        player1 = AudioPlayer(file: file1!)
        player2 = AudioPlayer(file: file2!)
        player1!.isLooping = true
        player2!.isLooping = true
        player3 = AudioPlayer(file: file3!)
        player4 = AudioPlayer(file: file4!)
        player3!.isLooping = true
        player4!.isLooping = true
    
        
        try? engine.start()
        engine.output = mixer
        
        
        
        
        interstitial = loadInterstitial()
        /*
        do {
            try AVAudioSession.sharedInstance().setCategory(
                AVAudioSession.Category.playAndRecord,
                options: [.defaultToSpeaker,.mixWithOthers])
                //success = true
            } catch _ {
        }*/
        
        
        
        let instrument = UserDefaults.standard.object(forKey: "selectedInstrument") as? String
        if instrument == "guitar"{
            selectedInstrument = 0
        }else if instrument == "ukulele"{
            selectedInstrument = 1
        }else if instrument == "bass"{
            selectedInstrument = 2
        }
        
        setSelectedInstrumentView(number: selectedInstrument)
        
        askPermissionIfNeeded()
        setInstrumentImg()
        instrumentView.alpha = 0
        selectInstrumentView()
        setGuitarBtn()
        setUkuleleBtn()
        setBassBtn()
       // setFrequencyView()
        setFrequencyDasboardView()
        setHelperFrequencyLabel()
        
        
        
        if autoTuner{
            autoTunerSwitch.isOn = true
            autoTunerLbl.textColor = .white
        }
        
        
        
        AppCache.sharedInstance.selectedIndex = 1
        isPremium = UserDefaults.standard.bool(forKey: "isPremium")
        if !isPremium{
            if UserDefaults.standard.object(forKey: "loginCounter") as! Int == 1{
                UserDefaults.standard.setValue(2, forKey: "loginCounter")
                self.performSegue(withIdentifier: "premium", sender: nil)
            }else{
                let number = UserDefaults.standard.object(forKey: "loginCounter") as! Int
                if number <= 3{
                    self.performSegue(withIdentifier: "premium", sender: nil)
                }else if number > 3 && number <= 9{
                    if number%2 == 0{
                        Timer.scheduledTimer(withTimeInterval: 2, repeats: false) { (timer) in
                            if self.interstitial.isReady{
                                self.interstitial.present(fromRootViewController: self)
                            }else{
                                print("hazir degil")
                            }
                        }
                    }else{
                        self.performSegue(withIdentifier: "premium", sender: nil)
                    }
                }else{
                    Timer.scheduledTimer(withTimeInterval: 2, repeats: false) { (timer) in
                        if self.interstitial.isReady{
                            self.interstitial.present(fromRootViewController: self)
                        }else{
                            print("hazir degil")
                        }
                    }
                }
            }
        }
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(activeTuner), name: .tunerNotf, object: nil)
        /*
        do{
            try AVAudioSession.sharedInstance().setCategory(.playAndRecord, mode: .videoRecording, options: [])
            
        }catch let error{
            print(error.localizedDescription)
        }
        */
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        tuner.delegate = self
        tuner.start()
        
        
    }
    
    
    func askPermissionIfNeeded() {
        switch AVAudioSession.sharedInstance().recordPermission {
        case .undetermined:
            //askMicrophoneAuthorization()
        print("ask mic")
        case .denied:
            let alert = UIAlertController(title: "Error", message: "Please allow microphone usage from settings", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Open settings", style: .default, handler: { action in
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        case .granted:
            print("granted")
            //goToNextStep()
    
        default:
            break
        }
    }
    
    @objc func activeTuner(){
        tuner.delegate = self
        tuner.start()
    }
    
    @objc func showAds(){
        if interstitial.isReady{
            interstitial.present(fromRootViewController: self)
        }else{
            print("hazir degil")
        }
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        tuner.stop()
    }
    
    @IBAction func testButton(_ sender: Any) {
        
        
        
        bombSoundEffect1?.play()
        bombSoundEffect2?.play()
        bombSoundEffect3?.play()
        bombSoundEffect4?.play()
        
        
        
        //player1?.play()
        //player2?.start()
        
        
           
    }
    
    func loadInterstitial() -> GADInterstitial{
        
        let interstitial = GADInterstitial(adUnitID: "ca-app-pub-8923345218307944/8574079696")
        interstitial.delegate = self
        interstitial.load(GADRequest())
        return interstitial
    }
    
    
    @IBAction func changeInstrumentButton(_ sender: Any) {
        if instrumentView.alpha == 0{
            UIView.animate(withDuration: 0.5) {
                self.instrumentView.alpha = 1
            }
        }else{
            UIView.animate(withDuration: 0.5) {
                self.instrumentView.alpha = 0
            }
        }
        view.bringSubviewToFront(instrumentView)
    }
    
    @IBAction func switchValueChanged(_ sender: Any) {
        
        
        if autoTunerSwitch.isOn{
            helperLbl.isHidden = true
            autoTuner = true
            autoTunerLbl.textColor = .white
        }else{
            autoTuner = false
            autoTunerLbl.textColor = #colorLiteral(red: 0.2940818071, green: 0.2941383123, blue: 0.2940782011, alpha: 1)
            
            selectedString = 1
            
            if selectedInstrument == 0 {
                guitarBtn1.backgroundColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
                guitarBtn1.setTitleColor(#colorLiteral(red: 0.06273303926, green: 0.06275205314, blue: 0.06273179501, alpha: 1), for: .normal)
                guitarBtn2.backgroundColor = .clear
                guitarBtn2.setTitleColor(.white, for: .normal)
                guitarBtn3.backgroundColor = .clear
                guitarBtn3.setTitleColor(.white, for: .normal)
                guitarBtn4.backgroundColor = .clear
                guitarBtn4.setTitleColor(.white, for: .normal)
                guitarBtn5.backgroundColor = .clear
                guitarBtn5.setTitleColor(.white, for: .normal)
                guitarBtn6.backgroundColor = .clear
                guitarBtn6.setTitleColor(.white, for: .normal)
                selectedNoteFrequency = guitarChordStruct[0].frequency
            
            }
            if selectedInstrument == 1 {
                ukuleleBtn1.backgroundColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
                ukuleleBtn1.setTitleColor(#colorLiteral(red: 0.06273303926, green: 0.06275205314, blue: 0.06273179501, alpha: 1), for: .normal)
                ukuleleBtn2.backgroundColor = .clear
                ukuleleBtn2.setTitleColor(.white, for: .normal)
                ukuleleBtn3.backgroundColor = .clear
                ukuleleBtn3.setTitleColor(.white, for: .normal)
                ukuleleBtn4.backgroundColor = .clear
                ukuleleBtn4.setTitleColor(.white, for: .normal)
                selectedNoteFrequency = ukuleleChordStruct[0].frequency
                
            }
            if selectedInstrument == 2 {
                bassBtn1.backgroundColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
                bassBtn1.setTitleColor(#colorLiteral(red: 0.06273303926, green: 0.06275205314, blue: 0.06273179501, alpha: 1), for: .normal)
                bassBtn2.backgroundColor = .clear
                bassBtn2.setTitleColor(.white, for: .normal)
                bassBtn3.backgroundColor = .clear
                bassBtn3.setTitleColor(.white, for: .normal)
                bassBtn4.backgroundColor = .clear
                bassBtn4.setTitleColor(.white, for: .normal)
                selectedNoteFrequency = bassChordStruct[0].frequency
                
            }
            //hzLbl.text = "\(selectedNoteFrequency)"
        }
    }
    
    func setFrequencyView(){
        frequencyView.frame = CGRect(x: view.frame.size.width / 2, y: 100, width: 3, height: 100)
        frequencyView.backgroundColor = .red
        view.addSubview(frequencyView)
    }
    
    func setFrequencyDasboardView(){
        
        if(["iPhone 5s", "iPhone 6", "iPhone 6s", "iPhone 7", "iPhone SE", "iPhone 8", "iPhone 12 Mini"]).contains(modelName){
            frequencyBoardImg.frame = CGRect(x: 24, y: autoTunerSwitch.frame.maxY + 20, width: view.frame.size.width - 44, height: noteLbl.frame.minY - autoTunerSwitch.frame.maxY - 20)
        }else{
            frequencyBoardImg.frame = CGRect(x: 24, y: autoTunerSwitch.frame.maxY + 75, width: view.frame.size.width - 48, height: noteLbl.frame.minY - autoTunerSwitch.frame.maxY - 100)
            
        }
         
       
 
        //frequencyBoardImg.frame = CGRect(x: 24, y: autoTunerLbl.frame.maxY + 5, width: view.frame.size.width - 44, height: noteLbl.frame.minY - autoTunerLbl.frame.maxY - 5)
        frequencyBoardImg.image = UIImage(named: "frequencyBoard")
        frequencyBoardImg.backgroundColor = .clear
        view.addSubview(frequencyBoardImg)
    }
    
    
    
    
    
    func tunerDidTick(pitch: TMPitch, delta: Double, frequencyValue: String, frekansInt: Int, amplitude: Double, frequencyDetailValue: Double) {
        //print("delta\(delta)")
      //  print("frekans\(frequencyValue)")
       // print("octave\(pitch.octave)")
        //print("frekans int\(frekansInt)")
      //  print("amplitude\(amplitude)")
       // print(pitch.note.note.rawValue)
        //print(pitch.note.accidental.rawValue)
        
        let frekans = delta
        
        if amplitude > 0.04{
            
            /*
            if autoTuner{
                hzLbl.text = "\(frequencyValue)"
            }
            */
            
            //belirlenen nota enstrumanda mevcutsa yakiyoruz
           
            //print(pitch.octave)
            print(pitch.note.note)
            
            if autoTuner{
                
                let frekans = Int(delta)
                
                
                if selectedInstrument == 0{
                    
                    if "\(pitch.note.note)" == guitarNoteArray[0].note && pitch.octave == guitarNoteArray[0].pitch && pitch.note.accidental.rawValue == 0{
                        guitarBtnFrequencySuccess(sender: 1)
                        setFrequencyImage(frequency: Int(frekans))
                        hzLbl.isHidden = true
                        noteLbl.isHidden = false
                        noteLbl.text = "\(pitch.note.note)"
                        setFrequencyImage(frequency: frekans)
                    }else if "\(pitch.note.note)" == guitarNoteArray[1].note && pitch.octave == guitarNoteArray[1].pitch && pitch.note.accidental.rawValue == 0{
                        guitarBtnFrequencySuccess(sender: 2)
                        setFrequencyImage(frequency: Int(frekans))
                        hzLbl.isHidden = true
                        noteLbl.isHidden = false
                        noteLbl.text = "\(pitch.note.note)"
                        setFrequencyImage(frequency: frekans)
                    }else if "\(pitch.note.note)" == guitarNoteArray[2].note && pitch.octave == guitarNoteArray[2].pitch && pitch.note.accidental.rawValue == 0{
                        guitarBtnFrequencySuccess(sender: 3)
                        setFrequencyImage(frequency: Int(frekans))
                        hzLbl.isHidden = true
                        noteLbl.isHidden = false
                        noteLbl.text = "\(pitch.note.note)"
                        setFrequencyImage(frequency: frekans)
                    }else if "\(pitch.note.note)" == guitarNoteArray[3].note && pitch.octave == guitarNoteArray[3].pitch && pitch.note.accidental.rawValue == 0{
                        guitarBtnFrequencySuccess(sender: 4)
                        setFrequencyImage(frequency: Int(frekans))
                        hzLbl.isHidden = true
                        noteLbl.isHidden = false
                        noteLbl.text = "\(pitch.note.note)"
                        setFrequencyImage(frequency: frekans)
                    }else if "\(pitch.note.note)" == guitarNoteArray[4].note && pitch.octave == guitarNoteArray[4].pitch && pitch.note.accidental.rawValue == 0{
                        guitarBtnFrequencySuccess(sender: 5)
                        setFrequencyImage(frequency: Int(frekans))
                        hzLbl.isHidden = true
                        noteLbl.isHidden = false
                        noteLbl.text = "\(pitch.note.note)"
                        setFrequencyImage(frequency: frekans)
                    }else if "\(pitch.note.note)" == guitarNoteArray[5].note && pitch.octave == guitarNoteArray[5].pitch && pitch.note.accidental.rawValue == 0{
                        guitarBtnFrequencySuccess(sender: 6)
                        setFrequencyImage(frequency: Int(frekans))
                        hzLbl.isHidden = true
                        noteLbl.isHidden = false
                        noteLbl.text = "\(pitch.note.note)"
                        setFrequencyImage(frequency: frekans)
                    }else{
                        frequencyBoardImg.image = UIImage(named: "frequencyBoard")
                        noteLbl.isHidden = true
                        hzLbl.isHidden = false
                        guitarBtn1.backgroundColor = .clear
                        guitarBtn1.setTitleColor(.white, for: .normal)
                        guitarBtn2.backgroundColor = .clear
                        guitarBtn2.setTitleColor(.white, for: .normal)
                        guitarBtn3.backgroundColor = .clear
                        guitarBtn3.setTitleColor(.white, for: .normal)
                        guitarBtn4.backgroundColor = .clear
                        guitarBtn4.setTitleColor(.white, for: .normal)
                        guitarBtn5.backgroundColor = .clear
                        guitarBtn5.setTitleColor(.white, for: .normal)
                        guitarBtn6.backgroundColor = .clear
                        guitarBtn6.setTitleColor(.white, for: .normal)
                    }
                      
                }
                if selectedInstrument == 1{
                    
                    if "\(pitch.note.note)" == ukuleleNoteArray[0].note && pitch.octave == ukuleleNoteArray[0].pitch && pitch.note.accidental.rawValue == 0{
                        ukuleleBtnFrequencySuccess(sender: 1)
                        hzLbl.isHidden = true
                        noteLbl.isHidden = false
                        noteLbl.text = "\(pitch.note.note)"
                        setFrequencyImage(frequency: frekans)
                    }else if "\(pitch.note.note)" == ukuleleNoteArray[1].note && pitch.octave == ukuleleNoteArray[1].pitch && pitch.note.accidental.rawValue == 0{
                        ukuleleBtnFrequencySuccess(sender: 2)
                        hzLbl.isHidden = true
                        noteLbl.isHidden = false
                        noteLbl.text = "\(pitch.note.note)"
                        setFrequencyImage(frequency: frekans)
                    }else if "\(pitch.note.note)" == ukuleleNoteArray[2].note && pitch.octave == ukuleleNoteArray[2].pitch && pitch.note.accidental.rawValue == 0{
                        ukuleleBtnFrequencySuccess(sender: 3)
                        hzLbl.isHidden = true
                        noteLbl.isHidden = false
                        noteLbl.text = "\(pitch.note.note)"
                        setFrequencyImage(frequency: frekans)
                    }else if "\(pitch.note.note)" == ukuleleNoteArray[3].note && pitch.octave == ukuleleNoteArray[3].pitch && pitch.note.accidental.rawValue == 0{
                        ukuleleBtnFrequencySuccess(sender: 4)
                        hzLbl.isHidden = true
                        noteLbl.isHidden = false
                        noteLbl.text = "\(pitch.note.note)"
                        setFrequencyImage(frequency: frekans)
                    }
                    else{
                        frequencyBoardImg.image = UIImage(named: "frequencyBoard")
                        noteLbl.isHidden = true
                        hzLbl.isHidden = false
                        ukuleleBtn1.backgroundColor = .clear
                        ukuleleBtn1.setTitleColor(.white, for: .normal)
                        ukuleleBtn2.backgroundColor = .clear
                        ukuleleBtn2.setTitleColor(.white, for: .normal)
                        ukuleleBtn3.backgroundColor = .clear
                        ukuleleBtn3.setTitleColor(.white, for: .normal)
                        ukuleleBtn4.backgroundColor = .clear
                        ukuleleBtn4.setTitleColor(.white, for: .normal)
                    }
                }
                
                if selectedInstrument == 2{
                    
                    if "\(pitch.note.note)" == bassNoteArray[0].note && pitch.octave == bassNoteArray[0].pitch && pitch.note.accidental.rawValue == 0{
                        bassBtnFrequencySuccess(sender: 1)
                        hzLbl.isHidden = true
                        noteLbl.isHidden = false
                        noteLbl.text = "\(pitch.note.note)"
                        setFrequencyImage(frequency: frekans)
                    }else if "\(pitch.note.note)" == bassNoteArray[1].note && pitch.octave == bassNoteArray[1].pitch && pitch.note.accidental.rawValue == 0{
                        bassBtnFrequencySuccess(sender: 2)
                        hzLbl.isHidden = true
                        noteLbl.isHidden = false
                        noteLbl.text = "\(pitch.note.note)"
                        setFrequencyImage(frequency: frekans)
                    }else if "\(pitch.note.note)" == bassNoteArray[2].note && pitch.octave == bassNoteArray[2].pitch && pitch.note.accidental.rawValue == 0{
                        bassBtnFrequencySuccess(sender: 3)
                        hzLbl.isHidden = true
                        noteLbl.isHidden = false
                        noteLbl.text = "\(pitch.note.note)"
                        setFrequencyImage(frequency: frekans)
                    }else if "\(pitch.note.note)" == bassNoteArray[3].note && pitch.octave == bassNoteArray[3].pitch && pitch.note.accidental.rawValue == 0{
                        bassBtnFrequencySuccess(sender: 4)
                        hzLbl.isHidden = true
                        noteLbl.isHidden = false
                        noteLbl.text = "\(pitch.note.note)"
                        setFrequencyImage(frequency: frekans)
                    }
                    else{
                        frequencyBoardImg.image = UIImage(named: "frequencyBoard")
                        noteLbl.isHidden = true
                        hzLbl.isHidden = false
                        bassBtn1.backgroundColor = .clear
                        bassBtn1.setTitleColor(.white, for: .normal)
                        bassBtn2.backgroundColor = .clear
                        bassBtn2.setTitleColor(.white, for: .normal)
                        bassBtn3.backgroundColor = .clear
                        bassBtn3.setTitleColor(.white, for: .normal)
                        bassBtn4.backgroundColor = .clear
                        bassBtn4.setTitleColor(.white, for: .normal)
                    }
                }
                
                
                
            }else{
                //manuel tuner aktifse
                
                
                if selectedInstrument == 0{
                    if selectedString == 1{
                        
                        targetFrequency = guitarChordStruct[0].frequency
                        noteLbl.text = guitarChordStruct[0].note
                        
                    }
                    if selectedString == 2{
                        
                        targetFrequency = guitarChordStruct[1].frequency
                        noteLbl.text = guitarChordStruct[1].note
                        
                    }
                    if selectedString == 3{
                        
                        targetFrequency = guitarChordStruct[2].frequency
                        noteLbl.text = guitarChordStruct[2].note
                        
                    }
                    if selectedString == 4{
                        
                        targetFrequency = guitarChordStruct[3].frequency
                        noteLbl.text = guitarChordStruct[3].note
                        
                    }
                    if selectedString == 5{
                        
                        targetFrequency = guitarChordStruct[4].frequency
                        noteLbl.text = guitarChordStruct[4].note
                        
                    }
                    if selectedString == 6{
                        
                        targetFrequency = guitarChordStruct[5].frequency
                        noteLbl.text = guitarChordStruct[5].note
                        
                    }
                }
                
                if selectedInstrument == 1{
                    if selectedString == 1{
                        targetFrequency = ukuleleChordStruct[0].frequency
                        noteLbl.text = ukuleleChordStruct[0].note
                    }
                    if selectedString == 2{
                        targetFrequency = ukuleleChordStruct[1].frequency
                        noteLbl.text = ukuleleChordStruct[1].note
                    }
                    if selectedString == 3{
                        targetFrequency = ukuleleChordStruct[2].frequency
                        noteLbl.text = ukuleleChordStruct[2].note
                    }
                    if selectedString == 4{
                        targetFrequency = bassChordStruct[3].frequency
                        noteLbl.text = bassChordStruct[3].note
                    }
                }
                
                if selectedInstrument == 2{
                    if selectedString == 1{
                        targetFrequency = bassChordStruct[0].frequency
                        noteLbl.text = bassChordStruct[0].note
                    }
                    if selectedString == 2{
                        targetFrequency = bassChordStruct[1].frequency
                        noteLbl.text = bassChordStruct[1].note
                    }
                    if selectedString == 3{
                        targetFrequency = bassChordStruct[2].frequency
                        noteLbl.text = bassChordStruct[2].note
                    }
                    if selectedString == 4{
                        targetFrequency = bassChordStruct[3].frequency
                        noteLbl.text = bassChordStruct[3].note
                    }
                }
                
                
                //print(Int(frekansInt))
                //print(Int(targetFrequency!))
                
               
                var value = Int(frekansInt) - Int(targetFrequency!)
                
                let frekans = Int(delta)
                
                
                if pitch.octave != 4{
                    if pitch.octave == 1{
                        value = value * 8
                    }
                    if pitch.octave == 0{
                        value = value * 16
                    }
                    if pitch.octave == 3{
                        value = value * 2
                    }
                    if pitch.octave == 2{
                        value = value * 4
                    }
                    
                    if pitch.octave == 5{
                        value = value / 2
                    }
                    if pitch.octave == 6{
                        value = value / 4
                    }
                    if pitch.octave == 7{
                        value = value / 8
                    }
                    if pitch.octave == 8{
                        value = value / 16
                    }
                    
                    
                }
                var positive = abs(value)
                print(positive)
                
                if positive < 50 && pitch.note.accidental.rawValue == 0{
                    setFrequencyImage(frequency: frekans)
                    helperLbl.isHidden = true
                    
                }else{
                    helperLbl.isHidden = false
                    if value < 0{
                        helperLbl.text = "too_low".localized
                        frequencyBoardImg.image = UIImage(named: "-12")
                    }else{
                        helperLbl.text = "too_high".localized
                        frequencyBoardImg.image = UIImage(named: "12")
                    }
                }
        
             
            }
            
            
            
            
            
            
            
            //frekans view konumlandirmasi
            let frekans = Int(delta)
            //let frameView = view.frame.maxX * 0.6
            let frameView = view.safeAreaLayoutGuide.layoutFrame.width * 0.9
            
            
             
            multiPosition(CGFloat(Int(frameView/2) + Int(Int(frameView/100) * frekans)), CGFloat(Int(frameView/2) + Int(Int(frameView/100) * frekans)) )
              
            if frekans > 0{
                frequencyView.frame.origin.x = CGFloat(Int(frameView/2) + Int(Int(frameView/100) * frekans))
            } else {
                let pozitif = abs(frekans)
                frequencyView.frame.origin.x = CGFloat(Int(frameView/2) - Int(Int(frameView/100) * pozitif))
                
            }
            if abs(delta) < tolerance {
                
            }
            
            
            
            
            
        }
        
    }
    
    
    func setFrequencyImage(frequency : Int){
        if frequency > -5 && frequency < 5{
            frequencyBoardImg.image = UIImage(named: "0")
        }
        if frequency <= -5 && frequency >= -14{
            frequencyBoardImg.image = UIImage(named: "-1")
        }
        if frequency < -14 && frequency >= -23{
            frequencyBoardImg.image = UIImage(named: "-2")
        }
        if frequency < -23 && frequency >= -32{
            frequencyBoardImg.image = UIImage(named: "-3")
        }
        if frequency < -32 && frequency >= -41{
            frequencyBoardImg.image = UIImage(named: "-4")
        }
        if frequency < -41 && frequency >= -50{
            frequencyBoardImg.image = UIImage(named: "-5")
        }
        if frequency >= 5  && frequency <= 14{
            frequencyBoardImg.image = UIImage(named: "1")
        }
        if frequency > 14  && frequency <= 23{
            frequencyBoardImg.image = UIImage(named: "2")
        }
        if frequency > 23  && frequency <= 32{
            frequencyBoardImg.image = UIImage(named: "3")
        }
        if frequency > 32  && frequency <= 41{
            frequencyBoardImg.image = UIImage(named: "4")
        }
        if frequency > 41  && frequency <= 50{
            frequencyBoardImg.image = UIImage(named: "5")
        }
        
        
    }
    
    func setHelperFrequencyLabel(){
        helperLbl.frame = CGRect(x: (view.frame.size.width - 200)/2, y: autoTunerSwitch.frame.midY, width: 200, height: 50)
       // helperLbl.text = "TOO HIGH!"
        helperLbl.textColor = .white
        helperLbl.font = UIFont(name: "Avenir-Medium", size: 18)
        helperLbl.textAlignment = .center
        view.addSubview(helperLbl)
        
        helperLbl.isHidden = true
    }
    
    
    func setInstrumentImg(){
        bottomView.frame = CGRect(x: 0, y: view.bounds.size.height * 0.55, width: view.bounds.size.width, height: view.bounds.size.height * 0.45)
        bottomView.backgroundColor = #colorLiteral(red: 0.06273303926, green: 0.06275205314, blue: 0.06273179501, alpha: 1)
        view.addSubview(bottomView)
        
        instrumentImg.frame = CGRect(x: view.bounds.size.width * 0.2, y: bottomView.frame.minY - 20, width: view.bounds.size.width * 0.6, height: view.bounds.size.height * 0.44)
        instrumentImg.contentMode = .scaleAspectFit
        view.addSubview(instrumentImg)
        
        hzLbl.frame = CGRect(x: (view.frame.size.width / 2) - 60, y: instrumentImg.frame.minY - 35, width: 120, height: 25)
        hzLbl.font = UIFont(name: "Avenir-Black", size: 18)
        hzLbl.textColor = .white
        hzLbl.text = "Hz"
        hzLbl.textAlignment = .center
        view.addSubview(hzLbl)
        
        //noteLbl.frame = CGRect(x: (view.frame.size.width / 2) - 60, y: instrumentImg.frame.minY - 110, width: 120, height: 100)
        noteLbl.frame = CGRect(x: (view.frame.size.width / 2) - 60, y: instrumentImg.frame.minY - 60, width: 120, height: 50)
        noteLbl.font = UIFont(name: "Avenir-Black", size: 40)
        noteLbl.textColor = #colorLiteral(red: 2.151550916e-06, green: 0.6969773173, blue: 0.371265173, alpha: 1)
        noteLbl.textAlignment = .center
        view.addSubview(noteLbl)
        noteLbl.isHidden = true
        
        let bemolImg = UIImageView(frame: CGRect(x: 23, y: instrumentImg.frame.minY - 35, width: 11, height: 28))
        bemolImg.image = UIImage(named: "bemol")
        bemolImg.contentMode = .scaleAspectFit
        view.addSubview(bemolImg)
        
        let diyezImg = UIImageView(frame: CGRect(x: view.bounds.size.width - 38 , y: instrumentImg.frame.minY - 38, width: 18, height: 36))
        diyezImg.image = UIImage(named: "diyez")
        diyezImg.contentMode = .scaleAspectFit
        view.addSubview(diyezImg)
    }
    
    
    func setGuitarBtn(){
        
        
        guitarBtn1.frame = CGRect(x: instrumentImg.frame.minX - 30, y: (bottomView.frame.size.height / 9) * 4.4, width: 40, height: 40)
        guitarBtn1.layer.cornerRadius = guitarBtn1.frame.size.width/2
        guitarBtn1.layer.borderWidth = 1
        guitarBtn1.layer.borderColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
        guitarBtn1.tag = 1
        guitarBtn1.addTarget(self, action: #selector(guitarBtnTapped), for: .touchUpInside)
        guitarBtn1.setTitle(guitarChordStruct[0].note, for: .normal)
        bottomView.addSubview(guitarBtn1)
        
        guitarBtn2.frame = CGRect(x: instrumentImg.frame.minX - 30, y: (bottomView.frame.size.height / 9) * 2.7, width: 40, height: 40)
        guitarBtn2.layer.cornerRadius = guitarBtn2.frame.size.width/2
        guitarBtn2.layer.borderWidth = 1
        guitarBtn2.layer.borderColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
        guitarBtn2.tag = 2
        guitarBtn2.addTarget(self, action: #selector(guitarBtnTapped), for: .touchUpInside)
        guitarBtn2.setTitle(guitarChordStruct[1].note, for: .normal)
        bottomView.addSubview(guitarBtn2)
        
        guitarBtn3.frame = CGRect(x: instrumentImg.frame.minX - 30, y: bottomView.frame.size.height / 9, width: 40, height: 40)
        guitarBtn3.layer.cornerRadius = guitarBtn3.frame.size.width/2
        guitarBtn3.layer.borderWidth = 1
        guitarBtn3.layer.borderColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
        guitarBtn3.tag = 3
        guitarBtn3.addTarget(self, action: #selector(guitarBtnTapped), for: .touchUpInside)
        guitarBtn3.setTitle(guitarChordStruct[2].note, for: .normal)
        bottomView.addSubview(guitarBtn3)
        
        guitarBtn4.frame = CGRect(x: instrumentImg.frame.maxX - 10 , y: bottomView.frame.size.height / 9, width: 40, height: 40)
        guitarBtn4.layer.cornerRadius = guitarBtn4.frame.size.width/2
        guitarBtn4.layer.borderWidth = 1
        guitarBtn4.layer.borderColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
        guitarBtn4.tag = 4
        guitarBtn4.addTarget(self, action: #selector(guitarBtnTapped), for: .touchUpInside)
        guitarBtn4.setTitle(guitarChordStruct[3].note, for: .normal)
        bottomView.addSubview(guitarBtn4)
        
        guitarBtn5.frame = CGRect(x: instrumentImg.frame.maxX - 10, y: (bottomView.frame.size.height / 9) * 2.7, width: 40, height: 40)
        guitarBtn5.layer.cornerRadius = guitarBtn5.frame.size.width/2
        guitarBtn5.layer.borderWidth = 1
        guitarBtn5.layer.borderColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
        guitarBtn5.tag = 5
        guitarBtn5.addTarget(self, action: #selector(guitarBtnTapped), for: .touchUpInside)
        guitarBtn5.setTitle(guitarChordStruct[4].note, for: .normal)
        bottomView.addSubview(guitarBtn5)
        
        guitarBtn6.frame = CGRect(x: instrumentImg.frame.maxX - 10, y: (bottomView.frame.size.height / 9) * 4.4, width: 40, height: 40)
        guitarBtn6.layer.cornerRadius = guitarBtn6.frame.size.width/2
        guitarBtn6.layer.borderWidth = 1
        guitarBtn6.layer.borderColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
        guitarBtn6.tag = 6
        guitarBtn6.addTarget(self, action: #selector(guitarBtnTapped), for: .touchUpInside)
        guitarBtn6.setTitle(guitarChordStruct[5].note, for: .normal)
        bottomView.addSubview(guitarBtn6)
    }
    
    
    @objc func guitarBtnTapped(sender : UIButton){
        switch sender.tag {
        case 1:
            let sound = setPlaySound(song: "SNG-E")
            sound.play()
            selectedString = 1
            guitarBtn1.backgroundColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
            guitarBtn1.setTitleColor(#colorLiteral(red: 0.06273303926, green: 0.06275205314, blue: 0.06273179501, alpha: 1), for: .normal)
            guitarBtn2.backgroundColor = .clear
            guitarBtn2.setTitleColor(.white, for: .normal)
            guitarBtn3.backgroundColor = .clear
            guitarBtn3.setTitleColor(.white, for: .normal)
            guitarBtn4.backgroundColor = .clear
            guitarBtn4.setTitleColor(.white, for: .normal)
            guitarBtn5.backgroundColor = .clear
            guitarBtn5.setTitleColor(.white, for: .normal)
            guitarBtn6.backgroundColor = .clear
            guitarBtn6.setTitleColor(.white, for: .normal)
            if autoTuner == false{
                selectedNoteFrequency = guitarChordStruct[0].frequency
                //hzLbl.text = "\(selectedNoteFrequency)"
            }
        case 2:
            let sound = setPlaySound(song: "SNG-A")
            sound.play()
            selectedString = 2
            guitarBtn2.backgroundColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
            guitarBtn2.setTitleColor(#colorLiteral(red: 0.06273303926, green: 0.06275205314, blue: 0.06273179501, alpha: 1), for: .normal)
            guitarBtn1.backgroundColor = .clear
            guitarBtn1.setTitleColor(.white, for: .normal)
            guitarBtn3.backgroundColor = .clear
            guitarBtn3.setTitleColor(.white, for: .normal)
            guitarBtn4.backgroundColor = .clear
            guitarBtn4.setTitleColor(.white, for: .normal)
            guitarBtn5.backgroundColor = .clear
            guitarBtn5.setTitleColor(.white, for: .normal)
            guitarBtn6.backgroundColor = .clear
            guitarBtn6.setTitleColor(.white, for: .normal)
            if autoTuner == false{
                selectedNoteFrequency = guitarChordStruct[1].frequency
                //hzLbl.text = "\(selectedNoteFrequency)"
            }
        case 3:
            let sound = setPlaySound(song: "SNG-D")
            sound.play()
            selectedString = 3
            guitarBtn3.backgroundColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
            guitarBtn3.setTitleColor(#colorLiteral(red: 0.06273303926, green: 0.06275205314, blue: 0.06273179501, alpha: 1), for: .normal)
            guitarBtn2.backgroundColor = .clear
            guitarBtn2.setTitleColor(.white, for: .normal)
            guitarBtn1.backgroundColor = .clear
            guitarBtn1.setTitleColor(.white, for: .normal)
            guitarBtn4.backgroundColor = .clear
            guitarBtn4.setTitleColor(.white, for: .normal)
            guitarBtn5.backgroundColor = .clear
            guitarBtn5.setTitleColor(.white, for: .normal)
            guitarBtn6.backgroundColor = .clear
            guitarBtn6.setTitleColor(.white, for: .normal)
            if autoTuner == false{
                selectedNoteFrequency = guitarChordStruct[2].frequency
                //hzLbl.text = "\(selectedNoteFrequency)"
            }
        case 4:
            let sound = setPlaySound(song: "SNG-G")
            sound.play()
            selectedString = 4
            guitarBtn4.backgroundColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
            guitarBtn4.setTitleColor(#colorLiteral(red: 0.06273303926, green: 0.06275205314, blue: 0.06273179501, alpha: 1), for: .normal)
            guitarBtn2.backgroundColor = .clear
            guitarBtn2.setTitleColor(.white, for: .normal)
            guitarBtn3.backgroundColor = .clear
            guitarBtn3.setTitleColor(.white, for: .normal)
            guitarBtn1.backgroundColor = .clear
            guitarBtn1.setTitleColor(.white, for: .normal)
            guitarBtn5.backgroundColor = .clear
            guitarBtn5.setTitleColor(.white, for: .normal)
            guitarBtn6.backgroundColor = .clear
            guitarBtn6.setTitleColor(.white, for: .normal)
            if autoTuner == false{
                selectedNoteFrequency = guitarChordStruct[3].frequency
               //hzLbl.text = "\(selectedNoteFrequency)"
            }
        case 5:
            let sound = setPlaySound(song: "SNG-B")
            sound.play()
            selectedString = 5
            guitarBtn5.backgroundColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
            guitarBtn5.setTitleColor(#colorLiteral(red: 0.06273303926, green: 0.06275205314, blue: 0.06273179501, alpha: 1), for: .normal)
            guitarBtn2.backgroundColor = .clear
            guitarBtn2.setTitleColor(.white, for: .normal)
            guitarBtn3.backgroundColor = .clear
            guitarBtn3.setTitleColor(.white, for: .normal)
            guitarBtn4.backgroundColor = .clear
            guitarBtn4.setTitleColor(.white, for: .normal)
            guitarBtn1.backgroundColor = .clear
            guitarBtn1.setTitleColor(.white, for: .normal)
            guitarBtn6.backgroundColor = .clear
            guitarBtn6.setTitleColor(.white, for: .normal)
            if autoTuner == false{
                selectedNoteFrequency = guitarChordStruct[4].frequency
                //hzLbl.text = "\(selectedNoteFrequency)"
            }
        case 6:
            let sound = setPlaySound(song: "SNG-E1")
            sound.play()
            selectedString = 6
            guitarBtn6.backgroundColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
            guitarBtn6.setTitleColor(#colorLiteral(red: 0.06273303926, green: 0.06275205314, blue: 0.06273179501, alpha: 1), for: .normal)
            guitarBtn2.backgroundColor = .clear
            guitarBtn2.setTitleColor(.white, for: .normal)
            guitarBtn3.backgroundColor = .clear
            guitarBtn3.setTitleColor(.white, for: .normal)
            guitarBtn4.backgroundColor = .clear
            guitarBtn4.setTitleColor(.white, for: .normal)
            guitarBtn5.backgroundColor = .clear
            guitarBtn5.setTitleColor(.white, for: .normal)
            guitarBtn1.backgroundColor = .clear
            guitarBtn1.setTitleColor(.white, for: .normal)
            if autoTuner == false{
                selectedNoteFrequency = guitarChordStruct[5].frequency
                //hzLbl.text = "\(selectedNoteFrequency)"
            }
        default:
            return
        }
        
        
    }
    
    func setUkuleleBtn(){
        ukuleleBtn1.frame = CGRect(x: instrumentImg.frame.minX - 40, y: (bottomView.frame.size.height / 6.2) * 2.8, width: 40, height: 40)
        
        ukuleleBtn1.layer.cornerRadius = ukuleleBtn1.frame.size.width/2
        ukuleleBtn1.layer.borderWidth = 1
        ukuleleBtn1.layer.borderColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
        ukuleleBtn1.tag = 1
        ukuleleBtn1.addTarget(self, action: #selector(ukuleleBtnTapped), for: .touchUpInside)
        ukuleleBtn1.setTitle(ukuleleChordStruct[0].note, for: .normal)
        bottomView.addSubview(ukuleleBtn1)
        
        ukuleleBtn2.frame = CGRect(x: instrumentImg.frame.minX - 50, y: (bottomView.frame.size.height / 6.2) * 1.1, width: 40, height: 40)
        ukuleleBtn2.layer.cornerRadius = ukuleleBtn2.frame.size.width/2
        ukuleleBtn2.layer.borderWidth = 1
        ukuleleBtn2.layer.borderColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
        ukuleleBtn2.tag = 2
        ukuleleBtn2.addTarget(self, action: #selector(ukuleleBtnTapped), for: .touchUpInside)
        ukuleleBtn2.setTitle(ukuleleChordStruct[1].note, for: .normal)
        bottomView.addSubview(ukuleleBtn2)
        
        ukuleleBtn3.frame = CGRect(x: instrumentImg.frame.maxX , y: (bottomView.frame.size.height / 6.2) * 2.8, width: 40, height: 40)
        ukuleleBtn3.layer.cornerRadius = ukuleleBtn3.frame.size.width/2
        ukuleleBtn3.layer.borderWidth = 1
        ukuleleBtn3.layer.borderColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
        ukuleleBtn3.tag = 3
        ukuleleBtn3.addTarget(self, action: #selector(ukuleleBtnTapped), for: .touchUpInside)
        ukuleleBtn3.setTitle(ukuleleChordStruct[3].note, for: .normal)
        bottomView.addSubview(ukuleleBtn3)
        
        ukuleleBtn4.frame = CGRect(x: instrumentImg.frame.maxX + 10, y: (bottomView.frame.size.height / 6.2) * 1.1, width: 40, height: 40)
        ukuleleBtn4.layer.cornerRadius = ukuleleBtn3.frame.size.width/2
        ukuleleBtn4.layer.borderWidth = 1
        ukuleleBtn4.layer.borderColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
        ukuleleBtn4.tag = 4
        ukuleleBtn4.addTarget(self, action: #selector(ukuleleBtnTapped), for: .touchUpInside)
        ukuleleBtn4.setTitle(ukuleleChordStruct[2].note, for: .normal)
        bottomView.addSubview(ukuleleBtn4)
    }
    
    @objc func ukuleleBtnTapped(sender : UIButton){
        switch sender.tag {
        case 1:
            let sound = setPlaySound(song: "SNU-G")
            sound.play()
            selectedString = 1
            ukuleleBtn1.backgroundColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
            ukuleleBtn1.setTitleColor(#colorLiteral(red: 0.06273303926, green: 0.06275205314, blue: 0.06273179501, alpha: 1), for: .normal)
            ukuleleBtn2.backgroundColor = .clear
            ukuleleBtn2.setTitleColor(.white, for: .normal)
            ukuleleBtn3.backgroundColor = .clear
            ukuleleBtn3.setTitleColor(.white, for: .normal)
            ukuleleBtn4.backgroundColor = .clear
            ukuleleBtn4.setTitleColor(.white, for: .normal)
            if autoTuner == false{
                selectedNoteFrequency = ukuleleChordStruct[0].frequency
                //hzLbl.text = "\(selectedNoteFrequency)"
            }
        case 2:
            let sound = setPlaySound(song: "SNU-C")
            sound.play()
            selectedString = 2
            ukuleleBtn2.backgroundColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
            ukuleleBtn2.setTitleColor(#colorLiteral(red: 0.06273303926, green: 0.06275205314, blue: 0.06273179501, alpha: 1), for: .normal)
            ukuleleBtn1.backgroundColor = .clear
            ukuleleBtn1.setTitleColor(.white, for: .normal)
            ukuleleBtn3.backgroundColor = .clear
            ukuleleBtn3.setTitleColor(.white, for: .normal)
            ukuleleBtn4.backgroundColor = .clear
            ukuleleBtn4.setTitleColor(.white, for: .normal)
            if autoTuner == false{
                selectedNoteFrequency = ukuleleChordStruct[1].frequency
                //hzLbl.text = "\(selectedNoteFrequency)"
            }
        case 3:
            let sound = setPlaySound(song: "SNU-E")
            sound.play()
            selectedString = 3
            ukuleleBtn3.backgroundColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
            ukuleleBtn3.setTitleColor(#colorLiteral(red: 0.06273303926, green: 0.06275205314, blue: 0.06273179501, alpha: 1), for: .normal)
            ukuleleBtn2.backgroundColor = .clear
            ukuleleBtn2.setTitleColor(.white, for: .normal)
            ukuleleBtn1.backgroundColor = .clear
            ukuleleBtn1.setTitleColor(.white, for: .normal)
            ukuleleBtn4.backgroundColor = .clear
            ukuleleBtn4.setTitleColor(.white, for: .normal)
            if autoTuner == false{
                selectedNoteFrequency = ukuleleChordStruct[2].frequency
                //hzLbl.text = "\(selectedNoteFrequency)"
            }
        case 4:
            let sound = setPlaySound(song: "SNU-A")
            sound.play()
            selectedString = 4
            ukuleleBtn4.backgroundColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
            ukuleleBtn4.setTitleColor(#colorLiteral(red: 0.06273303926, green: 0.06275205314, blue: 0.06273179501, alpha: 1), for: .normal)
            ukuleleBtn1.backgroundColor = .clear
            ukuleleBtn1.setTitleColor(.white, for: .normal)
            ukuleleBtn2.backgroundColor = .clear
            ukuleleBtn2.setTitleColor(.white, for: .normal)
            ukuleleBtn3.backgroundColor = .clear
            ukuleleBtn3.setTitleColor(.white, for: .normal)
            if autoTuner == false{
                selectedNoteFrequency = ukuleleChordStruct[2].frequency
                //hzLbl.text = "\(selectedNoteFrequency)"
            }
        default:
            return
        }
        
        
    }
    
    func setBassBtn(){
        bassBtn1.frame = CGRect(x: instrumentImg.frame.minX - 25, y: bottomView.frame.size.height * 0.45, width: 40, height: 40)
        bassBtn1.layer.cornerRadius = bassBtn1.frame.size.width/2
        bassBtn1.layer.borderWidth = 1
        bassBtn1.layer.borderColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
        bassBtn1.tag = 1
        bassBtn1.addTarget(self, action: #selector(bassBtnTapped(sender:)), for: .touchUpInside)
        bassBtn1.setTitle(bassChordStruct[0].note, for: .normal)
        bottomView.addSubview(bassBtn1)
        
        bassBtn2.frame = CGRect(x: instrumentImg.frame.minX - 10, y: bottomView.frame.size.height * 0.3, width: 40, height: 40)
        bassBtn2.layer.cornerRadius = bassBtn2.frame.size.width/2
        bassBtn2.layer.borderWidth = 1
        bassBtn2.layer.borderColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
        bassBtn2.tag = 2
        bassBtn2.addTarget(self, action: #selector(bassBtnTapped(sender:)), for: .touchUpInside)
        bassBtn2.setTitle(bassChordStruct[1].note, for: .normal)
        bottomView.addSubview(bassBtn2)
        
        bassBtn3.frame = CGRect(x: instrumentImg.frame.minX + 5 , y: bottomView.frame.size.height * 0.15, width: 40, height: 40)
        bassBtn3.layer.cornerRadius = bassBtn3.frame.size.width/2
        bassBtn3.layer.borderWidth = 1
        bassBtn3.layer.borderColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
        bassBtn3.tag = 3
        bassBtn3.addTarget(self, action: #selector(bassBtnTapped(sender:)), for: .touchUpInside)
        bassBtn3.setTitle(bassChordStruct[2].note, for: .normal)
        bottomView.addSubview(bassBtn3)
        
        bassBtn4.frame = CGRect(x: instrumentImg.frame.minX + 20 , y: 0, width: 40, height: 40)
        bassBtn4.layer.cornerRadius = bassBtn4.frame.size.width/2
        bassBtn4.layer.borderWidth = 1
        bassBtn4.layer.borderColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
        bassBtn4.tag = 4
        bassBtn4.addTarget(self, action: #selector(bassBtnTapped(sender:)), for: .touchUpInside)
        bassBtn4.setTitle(bassChordStruct[3].note, for: .normal)
        bottomView.addSubview(bassBtn4)
    }
    
    
    @objc func bassBtnTapped(sender : UIButton){
        switch sender.tag {
        case 1:
            let sound = setPlaySound(song: "SNB-E")
            sound.play()
            selectedString = 1
            bassBtn1.backgroundColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
            bassBtn1.setTitleColor(#colorLiteral(red: 0.06273303926, green: 0.06275205314, blue: 0.06273179501, alpha: 1), for: .normal)
            bassBtn2.backgroundColor = .clear
            bassBtn2.setTitleColor(.white, for: .normal)
            bassBtn3.backgroundColor = .clear
            bassBtn3.setTitleColor(.white, for: .normal)
            bassBtn4.backgroundColor = .clear
            bassBtn4.setTitleColor(.white, for: .normal)
            if autoTuner == false{
                selectedNoteFrequency = bassChordStruct[0].frequency
                //hzLbl.text = "\(selectedNoteFrequency)"
            }
        case 2:
            let sound = setPlaySound(song: "SNB-A")
            sound.play()
            selectedString = 2
            bassBtn2.backgroundColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
            bassBtn2.setTitleColor(#colorLiteral(red: 0.06273303926, green: 0.06275205314, blue: 0.06273179501, alpha: 1), for: .normal)
            bassBtn1.backgroundColor = .clear
            bassBtn1.setTitleColor(.white, for: .normal)
            bassBtn3.backgroundColor = .clear
            bassBtn3.setTitleColor(.white, for: .normal)
            bassBtn4.backgroundColor = .clear
            bassBtn4.setTitleColor(.white, for: .normal)
            if autoTuner == false{
                selectedNoteFrequency = bassChordStruct[1].frequency
                //hzLbl.text = "\(selectedNoteFrequency)"
            }
        case 3:
            let sound = setPlaySound(song: "SNB-D")
            sound.play()
            selectedString = 3
            bassBtn3.backgroundColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
            bassBtn3.setTitleColor(#colorLiteral(red: 0.06273303926, green: 0.06275205314, blue: 0.06273179501, alpha: 1), for: .normal)
            bassBtn2.backgroundColor = .clear
            bassBtn2.setTitleColor(.white, for: .normal)
            bassBtn1.backgroundColor = .clear
            bassBtn1.setTitleColor(.white, for: .normal)
            bassBtn4.backgroundColor = .clear
            bassBtn4.setTitleColor(.white, for: .normal)
            if autoTuner == false{
                selectedNoteFrequency = bassChordStruct[2].frequency
                //hzLbl.text = "\(selectedNoteFrequency)"
            }
        case 4:
            let sound = setPlaySound(song: "SNB-G")
            sound.play()
            selectedString = 4
            bassBtn4.backgroundColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
            bassBtn4.setTitleColor(#colorLiteral(red: 0.06273303926, green: 0.06275205314, blue: 0.06273179501, alpha: 1), for: .normal)
            bassBtn2.backgroundColor = .clear
            bassBtn2.setTitleColor(.white, for: .normal)
            bassBtn3.backgroundColor = .clear
            bassBtn3.setTitleColor(.white, for: .normal)
            bassBtn1.backgroundColor = .clear
            bassBtn1.setTitleColor(.white, for: .normal)
            if autoTuner == false{
                selectedNoteFrequency = bassChordStruct[3].frequency
                //hzLbl.text = "\(selectedNoteFrequency)"
            }
        default:
            return
        }
    }
    
    func selectInstrumentView(){
        instrumentView.frame = CGRect(x: 0, y: 70, width: view.bounds.size.width, height: 92)
        instrumentView.backgroundColor = #colorLiteral(red: 0.18124035, green: 0.1763195395, blue: 0.17636621, alpha: 1)
        view.addSubview(instrumentView)
        let size = instrumentView.frame.size
        let guitarImgBtn = UIButton(frame: CGRect(x: 24, y: 11, width: 60, height: 60))
        guitarImgBtn.tag = 0
        if selectedInstrument == 0{
            guitarImgBtn.layer.borderWidth = 2
            guitarImgBtn.layer.borderColor = #colorLiteral(red: 2.151550916e-06, green: 0.6969773173, blue: 0.371265173, alpha: 1)
        }else{
            guitarImgBtn.layer.borderWidth = 2
            guitarImgBtn.layer.borderColor = #colorLiteral(red: 0.2940818071, green: 0.2941383123, blue: 0.2940782011, alpha: 1)
        }
        guitarImgBtn.layer.cornerRadius = guitarImgBtn.frame.size.width / 2
        guitarImgBtn.addTarget(self, action: #selector(selectInstrument(sender:)), for: .touchUpInside)
        guitarImgBtn.setImage(UIImage(named: "kguitar"), for: .normal)
        instrumentView.addSubview(guitarImgBtn)
        
        let ukuleImgBtn = UIButton(frame: CGRect(x: ((instrumentView.frame.size.width - 228)/2) + 84, y: 11, width: 60, height: 60))
        if selectedInstrument == 1{
            ukuleImgBtn.layer.borderWidth = 2
            ukuleImgBtn.layer.borderColor = #colorLiteral(red: 2.151550916e-06, green: 0.6969773173, blue: 0.371265173, alpha: 1)
        }else{
            ukuleImgBtn.layer.borderWidth = 2
            ukuleImgBtn.layer.borderColor = #colorLiteral(red: 0.2940818071, green: 0.2941383123, blue: 0.2940782011, alpha: 1)
        }
        ukuleImgBtn.layer.cornerRadius = ukuleImgBtn.frame.size.width / 2
        ukuleImgBtn.tag = 1
        ukuleImgBtn.addTarget(self, action: #selector(selectInstrument(sender:)), for: .touchUpInside)
        ukuleImgBtn.setImage(UIImage(named: "kukulele"), for: .normal)
        instrumentView.addSubview(ukuleImgBtn)
        
        let bassImgBtn = UIButton(frame: CGRect(x: instrumentView.frame.size.width - 84, y: 11, width: 60, height: 60))
        if selectedInstrument == 2{
            bassImgBtn.layer.borderWidth = 2
            bassImgBtn.layer.borderColor = #colorLiteral(red: 2.151550916e-06, green: 0.6969773173, blue: 0.371265173, alpha: 1)
        }else{
            bassImgBtn.layer.borderWidth = 2
            bassImgBtn.layer.borderColor = #colorLiteral(red: 0.2940818071, green: 0.2941383123, blue: 0.2940782011, alpha: 1)
        }
        bassImgBtn.layer.cornerRadius = bassImgBtn.frame.size.width / 2
        bassImgBtn.tag = 2
        bassImgBtn.addTarget(self, action: #selector(selectInstrument(sender:)), for: .touchUpInside)
        bassImgBtn.setImage(UIImage(named: "kbass"), for: .normal)
        instrumentView.addSubview(bassImgBtn)
        
        let guitarLbl = UILabel(frame: CGRect(x: 24, y: guitarImgBtn.frame.maxY + 2, width: 60, height: 22))
        guitarLbl.text = "Guitar"
        guitarLbl.font = UIFont(name: "Avenir-Black", size: 12)
        guitarLbl.textColor = .white
        guitarLbl.textAlignment = .center
        instrumentView.addSubview(guitarLbl)
        
        let ukuleleLbl = UILabel(frame: CGRect(x: ukuleImgBtn.frame.minX, y: guitarImgBtn.frame.maxY + 2, width: 60, height: 22))
        ukuleleLbl.text = "Ukulele"
        ukuleleLbl.font = UIFont(name: "Avenir-Black", size: 12)
        ukuleleLbl.textColor = .white
        ukuleleLbl.textAlignment = .center
        instrumentView.addSubview(ukuleleLbl)
        
        let bassLbl = UILabel(frame: CGRect(x: bassImgBtn.frame.minX - 10, y: guitarImgBtn.frame.maxY + 2, width: 80, height: 22))
        bassLbl.text = "Bass Guitar"
        bassLbl.font = UIFont(name: "Avenir-Black", size: 12)
        bassLbl.textColor = .white
        bassLbl.textAlignment = .center
        instrumentView.addSubview(bassLbl)
        
    }
    
    func setPlaySound(song : String) -> AVAudioPlayer{
        
        var test1 : NSURL!
        test1 = NSURL(fileURLWithPath: Bundle.main.path(forResource: song, ofType: "wav")!)
        
        test = try? AVAudioPlayer(contentsOf: test1 as URL)
        test.prepareToPlay()
        return test
        
    }
    
    @objc func selectInstrument(sender:UIButton){
        
        autoTunerSwitch.isOn = true
        autoTuner = true
        if sender.tag == 0{
            UserDefaults.standard.set("guitar", forKey: "selectedInstrument")
        }else if sender.tag == 1{
            UserDefaults.standard.set("ukulele", forKey: "selectedInstrument")
        }else if sender.tag == 2{
            UserDefaults.standard.set("bass", forKey: "selectedInstrument")
        }
        
        selectedInstrument = sender.tag
        self.selectInstrumentView()
        setSelectedInstrumentView(number: sender.tag)
        UIView.animate(withDuration: 0.5) {
            self.instrumentView.alpha = 0
            
        }
        
    }
    
    
    func setSelectedInstrumentView(number : Int){
        switch number {
        case 0:
            topInstrumentBtn.setTitle("Guitar", for: .normal)
            instrumentImg.image = UIImage(named: "guitar1")
            guitarBtn1.isHidden = false
            guitarBtn2.isHidden = false
            guitarBtn3.isHidden = false
            guitarBtn4.isHidden = false
            guitarBtn5.isHidden = false
            guitarBtn6.isHidden = false
            ukuleleBtn1.isHidden = true
            ukuleleBtn2.isHidden = true
            ukuleleBtn3.isHidden = true
            ukuleleBtn4.isHidden = true
            bassBtn1.isHidden = true
            bassBtn2.isHidden = true
            bassBtn3.isHidden = true
            bassBtn4.isHidden = true
        case 1:
            topInstrumentBtn.setTitle("Ukulele", for: .normal)
            instrumentImg.image = UIImage(named: "ukulele1")
            guitarBtn1.isHidden = true
            guitarBtn2.isHidden = true
            guitarBtn3.isHidden = true
            guitarBtn4.isHidden = true
            guitarBtn5.isHidden = true
            guitarBtn6.isHidden = true
            ukuleleBtn1.isHidden = false
            ukuleleBtn2.isHidden = false
            ukuleleBtn3.isHidden = false
            ukuleleBtn4.isHidden = false
            bassBtn1.isHidden = true
            bassBtn2.isHidden = true
            bassBtn3.isHidden = true
            bassBtn4.isHidden = true
        case 2:
            topInstrumentBtn.setTitle("Bass Guitar", for: .normal)
            instrumentImg.image = UIImage(named: "bass1")
            guitarBtn1.isHidden = true
            guitarBtn2.isHidden = true
            guitarBtn3.isHidden = true
            guitarBtn4.isHidden = true
            guitarBtn5.isHidden = true
            guitarBtn6.isHidden = true
            ukuleleBtn1.isHidden = true
            ukuleleBtn2.isHidden = true
            ukuleleBtn3.isHidden = true
            ukuleleBtn4.isHidden = true
            bassBtn1.isHidden = false
            bassBtn2.isHidden = false
            bassBtn3.isHidden = false
            bassBtn4.isHidden = false
            
        default:
            return
        }
    }
    
    @objc func guitarBtnFrequencySuccess(sender : Int){
        switch sender {
        case 1:
            guitarBtn1.backgroundColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
            guitarBtn1.setTitleColor(#colorLiteral(red: 0.06273303926, green: 0.06275205314, blue: 0.06273179501, alpha: 1), for: .normal)
            guitarBtn2.backgroundColor = .clear
            guitarBtn2.setTitleColor(.white, for: .normal)
            guitarBtn3.backgroundColor = .clear
            guitarBtn3.setTitleColor(.white, for: .normal)
            guitarBtn4.backgroundColor = .clear
            guitarBtn4.setTitleColor(.white, for: .normal)
            guitarBtn5.backgroundColor = .clear
            guitarBtn5.setTitleColor(.white, for: .normal)
            guitarBtn6.backgroundColor = .clear
            guitarBtn6.setTitleColor(.white, for: .normal)
            if autoTuner == false{
                selectedNoteFrequency = guitarChordStruct[0].frequency
                hzLbl.text = "\(selectedNoteFrequency)"
            }
        case 2:
            guitarBtn2.backgroundColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
            guitarBtn2.setTitleColor(#colorLiteral(red: 0.06273303926, green: 0.06275205314, blue: 0.06273179501, alpha: 1), for: .normal)
            guitarBtn1.backgroundColor = .clear
            guitarBtn1.setTitleColor(.white, for: .normal)
            guitarBtn3.backgroundColor = .clear
            guitarBtn3.setTitleColor(.white, for: .normal)
            guitarBtn4.backgroundColor = .clear
            guitarBtn4.setTitleColor(.white, for: .normal)
            guitarBtn5.backgroundColor = .clear
            guitarBtn5.setTitleColor(.white, for: .normal)
            guitarBtn6.backgroundColor = .clear
            guitarBtn6.setTitleColor(.white, for: .normal)
            if autoTuner == false{
                selectedNoteFrequency = guitarChordStruct[1].frequency
                hzLbl.text = "\(selectedNoteFrequency)"
            }
        case 3:
            guitarBtn3.backgroundColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
            guitarBtn3.setTitleColor(#colorLiteral(red: 0.06273303926, green: 0.06275205314, blue: 0.06273179501, alpha: 1), for: .normal)
            guitarBtn2.backgroundColor = .clear
            guitarBtn2.setTitleColor(.white, for: .normal)
            guitarBtn1.backgroundColor = .clear
            guitarBtn1.setTitleColor(.white, for: .normal)
            guitarBtn4.backgroundColor = .clear
            guitarBtn4.setTitleColor(.white, for: .normal)
            guitarBtn5.backgroundColor = .clear
            guitarBtn5.setTitleColor(.white, for: .normal)
            guitarBtn6.backgroundColor = .clear
            guitarBtn6.setTitleColor(.white, for: .normal)
            if autoTuner == false{
                selectedNoteFrequency = guitarChordStruct[2].frequency
                hzLbl.text = "\(selectedNoteFrequency)"
            }
        case 4:
            guitarBtn4.backgroundColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
            guitarBtn4.setTitleColor(#colorLiteral(red: 0.06273303926, green: 0.06275205314, blue: 0.06273179501, alpha: 1), for: .normal)
            guitarBtn2.backgroundColor = .clear
            guitarBtn2.setTitleColor(.white, for: .normal)
            guitarBtn3.backgroundColor = .clear
            guitarBtn3.setTitleColor(.white, for: .normal)
            guitarBtn1.backgroundColor = .clear
            guitarBtn1.setTitleColor(.white, for: .normal)
            guitarBtn5.backgroundColor = .clear
            guitarBtn5.setTitleColor(.white, for: .normal)
            guitarBtn6.backgroundColor = .clear
            guitarBtn6.setTitleColor(.white, for: .normal)
            if autoTuner == false{
                selectedNoteFrequency = guitarChordStruct[3].frequency
                hzLbl.text = "\(selectedNoteFrequency)"
            }
        case 5:
            guitarBtn5.backgroundColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
            guitarBtn5.setTitleColor(#colorLiteral(red: 0.06273303926, green: 0.06275205314, blue: 0.06273179501, alpha: 1), for: .normal)
            guitarBtn2.backgroundColor = .clear
            guitarBtn2.setTitleColor(.white, for: .normal)
            guitarBtn3.backgroundColor = .clear
            guitarBtn3.setTitleColor(.white, for: .normal)
            guitarBtn4.backgroundColor = .clear
            guitarBtn4.setTitleColor(.white, for: .normal)
            guitarBtn1.backgroundColor = .clear
            guitarBtn1.setTitleColor(.white, for: .normal)
            guitarBtn6.backgroundColor = .clear
            guitarBtn6.setTitleColor(.white, for: .normal)
            if autoTuner == false{
                selectedNoteFrequency = guitarChordStruct[4].frequency
                hzLbl.text = "\(selectedNoteFrequency)"
            }
        case 6:
            guitarBtn6.backgroundColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
            guitarBtn6.setTitleColor(#colorLiteral(red: 0.06273303926, green: 0.06275205314, blue: 0.06273179501, alpha: 1), for: .normal)
            guitarBtn2.backgroundColor = .clear
            guitarBtn2.setTitleColor(.white, for: .normal)
            guitarBtn3.backgroundColor = .clear
            guitarBtn3.setTitleColor(.white, for: .normal)
            guitarBtn4.backgroundColor = .clear
            guitarBtn4.setTitleColor(.white, for: .normal)
            guitarBtn5.backgroundColor = .clear
            guitarBtn5.setTitleColor(.white, for: .normal)
            guitarBtn1.backgroundColor = .clear
            guitarBtn1.setTitleColor(.white, for: .normal)
            if autoTuner == false{
                selectedNoteFrequency = guitarChordStruct[5].frequency
                hzLbl.text = "\(selectedNoteFrequency)"
            }
        default:
            return
        }
        
        
    }
    
    
    func bassBtnFrequencySuccess(sender : Int){
        switch sender {
        case 1:
            bassBtn1.backgroundColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
            bassBtn1.setTitleColor(#colorLiteral(red: 0.06273303926, green: 0.06275205314, blue: 0.06273179501, alpha: 1), for: .normal)
            bassBtn2.backgroundColor = .clear
            bassBtn2.setTitleColor(.white, for: .normal)
            bassBtn3.backgroundColor = .clear
            bassBtn3.setTitleColor(.white, for: .normal)
            bassBtn4.backgroundColor = .clear
            bassBtn4.setTitleColor(.white, for: .normal)
            if autoTuner == false{
                selectedNoteFrequency = bassChordStruct[0].frequency
                hzLbl.text = "\(selectedNoteFrequency)"
            }
        case 2:
            bassBtn2.backgroundColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
            bassBtn2.setTitleColor(#colorLiteral(red: 0.06273303926, green: 0.06275205314, blue: 0.06273179501, alpha: 1), for: .normal)
            bassBtn1.backgroundColor = .clear
            bassBtn1.setTitleColor(.white, for: .normal)
            bassBtn3.backgroundColor = .clear
            bassBtn3.setTitleColor(.white, for: .normal)
            bassBtn4.backgroundColor = .clear
            bassBtn4.setTitleColor(.white, for: .normal)
            if autoTuner == false{
                selectedNoteFrequency = bassChordStruct[1].frequency
                hzLbl.text = "\(selectedNoteFrequency)"
            }
        case 3:
            bassBtn3.backgroundColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
            bassBtn3.setTitleColor(#colorLiteral(red: 0.06273303926, green: 0.06275205314, blue: 0.06273179501, alpha: 1), for: .normal)
            bassBtn2.backgroundColor = .clear
            bassBtn2.setTitleColor(.white, for: .normal)
            bassBtn1.backgroundColor = .clear
            bassBtn1.setTitleColor(.white, for: .normal)
            bassBtn4.backgroundColor = .clear
            bassBtn4.setTitleColor(.white, for: .normal)
            if autoTuner == false{
                selectedNoteFrequency = bassChordStruct[2].frequency
                hzLbl.text = "\(selectedNoteFrequency)"
            }
        case 4:
            bassBtn4.backgroundColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
            bassBtn4.setTitleColor(#colorLiteral(red: 0.06273303926, green: 0.06275205314, blue: 0.06273179501, alpha: 1), for: .normal)
            bassBtn2.backgroundColor = .clear
            bassBtn2.setTitleColor(.white, for: .normal)
            bassBtn3.backgroundColor = .clear
            bassBtn3.setTitleColor(.white, for: .normal)
            bassBtn1.backgroundColor = .clear
            bassBtn1.setTitleColor(.white, for: .normal)
            if autoTuner == false{
                selectedNoteFrequency = bassChordStruct[3].frequency
                hzLbl.text = "\(selectedNoteFrequency)"
            }
        default:
            return
        }
    }
    
    @objc func ukuleleBtnFrequencySuccess(sender : Int){
        switch sender {
        case 1:
            ukuleleBtn1.backgroundColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
            ukuleleBtn1.setTitleColor(#colorLiteral(red: 0.06273303926, green: 0.06275205314, blue: 0.06273179501, alpha: 1), for: .normal)
            ukuleleBtn2.backgroundColor = .clear
            ukuleleBtn2.setTitleColor(.white, for: .normal)
            ukuleleBtn3.backgroundColor = .clear
            ukuleleBtn3.setTitleColor(.white, for: .normal)
            ukuleleBtn4.backgroundColor = .clear
            ukuleleBtn4.setTitleColor(.white, for: .normal)
            if autoTuner == false{
                selectedNoteFrequency = ukuleleChordStruct[0].frequency
                hzLbl.text = "\(selectedNoteFrequency)"
            }
        case 2:
            ukuleleBtn2.backgroundColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
            ukuleleBtn2.setTitleColor(#colorLiteral(red: 0.06273303926, green: 0.06275205314, blue: 0.06273179501, alpha: 1), for: .normal)
            ukuleleBtn1.backgroundColor = .clear
            ukuleleBtn1.setTitleColor(.white, for: .normal)
            ukuleleBtn3.backgroundColor = .clear
            ukuleleBtn3.setTitleColor(.white, for: .normal)
            ukuleleBtn4.backgroundColor = .clear
            ukuleleBtn4.setTitleColor(.white, for: .normal)
            if autoTuner == false{
                selectedNoteFrequency = ukuleleChordStruct[1].frequency
                hzLbl.text = "\(selectedNoteFrequency)"
            }
        case 3:
            ukuleleBtn3.backgroundColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
            ukuleleBtn3.setTitleColor(#colorLiteral(red: 0.06273303926, green: 0.06275205314, blue: 0.06273179501, alpha: 1), for: .normal)
            ukuleleBtn2.backgroundColor = .clear
            ukuleleBtn2.setTitleColor(.white, for: .normal)
            ukuleleBtn1.backgroundColor = .clear
            ukuleleBtn1.setTitleColor(.white, for: .normal)
            ukuleleBtn4.backgroundColor = .clear
            ukuleleBtn4.setTitleColor(.white, for: .normal)
            if autoTuner == false{
                selectedNoteFrequency = ukuleleChordStruct[2].frequency
                hzLbl.text = "\(selectedNoteFrequency)"
            }
        case 4:
            ukuleleBtn4.backgroundColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
            ukuleleBtn4.setTitleColor(#colorLiteral(red: 0.06273303926, green: 0.06275205314, blue: 0.06273179501, alpha: 1), for: .normal)
            ukuleleBtn1.backgroundColor = .clear
            ukuleleBtn1.setTitleColor(.white, for: .normal)
            ukuleleBtn2.backgroundColor = .clear
            ukuleleBtn2.setTitleColor(.white, for: .normal)
            ukuleleBtn3.backgroundColor = .clear
            ukuleleBtn3.setTitleColor(.white, for: .normal)
            if autoTuner == false{
                selectedNoteFrequency = ukuleleChordStruct[2].frequency
                hzLbl.text = "\(selectedNoteFrequency)"
            }
        default:
            return
        }
        
        
    }
    
    
    
    fileprivate func multiPosition(_ firstPos: CGFloat, _ secondPos: CGFloat) {
      func simplePosition(_ pos: CGFloat) {
         UIView.animate(withDuration: 0.1, animations: {
           self.frequencyView.frame.origin.x = pos
        }, completion: nil)
      }
      
     UIView.animate(withDuration: 0.1, animations: {
       self.frequencyView.frame.origin.x = firstPos
        }, completion: { finished in
          simplePosition(secondPos)
      })
    }
    

}


extension TunerViewController : GADInterstitialDelegate{
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        //hata oldugunda
    }
    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
        //kullaniciya gosterildiginde
    }
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        //carpiya basildiginda
        interstitial = self.loadInterstitial()
        tuner.start()
    }
    
}



struct chordStruct {
    var note : String
    var frequency : Float
}

struct notePitchStruct {
    var note : String
    var pitch : Int
}


