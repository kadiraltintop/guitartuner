//
//  InstrumentSelectViewController.swift
//  Guitar Tuner
//
//  Created by Dev on 5.12.2020.
//

import UIKit
import FirebaseAnalytics

class InstrumentSelectViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var instrumentImgArray = [UIImage(named: "guitar"), UIImage(named: "ukulele"), UIImage(named: "bass")]
    var instrumentArray = ["guitar".localized, "ukulele".localized, "bass_guitar".localized]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("instrument")

        collectionView.delegate = self
        collectionView.dataSource = self
        // Do any additional setup after loading the view.
    }
    

    

}

extension InstrumentSelectViewController : UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return instrumentImgArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "instrumentCell", for: indexPath) as! InstrumentCollectionViewCell
        
        cell.instrumentLbl.frame = CGRect(x: 0, y: 6, width: 100, height: 24)
        let size = collectionView.frame.size
        cell.instrumentImg.frame = CGRect(x: 0, y: 36, width: size.width, height: cell.frame.size.height - 40)
        cell.instrumentImg.layer.cornerRadius = 10
        cell.instrumentImg.image = instrumentImgArray[indexPath.row]
        cell.instrumentLbl.text = instrumentArray[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0{
            UserDefaults.standard.setValue("guitar", forKey: "selectedInstrument")
            Analytics.logEvent("choose_guitar", parameters: [:])
        }
        if indexPath.row == 1{
            UserDefaults.standard.setValue("ukulele", forKey: "selectedInstrument")
            Analytics.logEvent("choose_ukulele", parameters: [:])
        }
        if indexPath.row == 2{
            UserDefaults.standard.setValue("bass", forKey: "selectedInstrument")
            Analytics.logEvent("choose_bass", parameters: [:])
        }
        self.performSegue(withIdentifier: "permission", sender: nil)
    }
}

extension InstrumentSelectViewController : UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = collectionView.frame.size
        return CGSize(width: (size.width), height: size.height / 3)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}





/*
 cell.instrumentLbl.frame = CGRect(x: 20, y: 20, width: 24, height: 100)
 let size = collectionView.frame.size
 cell.instrumentImg.frame = CGRect(x: 20, y: 50, width: size.width - 40, height: size.height - 70)
 */
