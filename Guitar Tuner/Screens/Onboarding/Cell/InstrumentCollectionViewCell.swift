//
//  InstrumentCollectionViewCell.swift
//  Guitar Tuner
//
//  Created by Dev on 5.12.2020.
//

import UIKit

class InstrumentCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var instrumentImg: UIImageView!
    @IBOutlet weak var instrumentLbl: UILabel!
}
