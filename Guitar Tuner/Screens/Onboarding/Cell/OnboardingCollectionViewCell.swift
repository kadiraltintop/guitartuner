//
//  OnboardingCollectionViewCell.swift
//  Guitar Tuner
//
//  Created by Dev on 5.12.2020.
//

import UIKit

class OnboardingCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var topLbl: UILabel!
    @IBOutlet weak var bottomLbl: UILabel!
    
}
