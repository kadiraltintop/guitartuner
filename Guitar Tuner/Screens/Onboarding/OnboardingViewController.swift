//
//  OnboardingViewController.swift
//  Guitar Tuner
//
//  Created by Dev on 5.12.2020.
//

import UIKit
import FirebaseAnalytics

class OnboardingViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var nextButton: UIButton!
    
    
    
    //"Enstumaninizi kolaylikla akort edin"
    //"Yuksek hassasiyetli ve kullanimi kolay tuner ile enstrumanini akort etmek artik cok kolay"
    
    //"Ensturman calmanizi kolaylastiracak bircok arac ile tanisin."
    //"Akor kutuphanesine erisin ve profesyoneller gibi metronom ile pratik yapin.
    
    var imgArray = [UIImage(named: "onboarding2"), UIImage(named: "onboarding3")]
    var bottomArray = ["onboarding_first_bottom".localized, "onboarding_second_bottom".localized ]
    var topArray = ["onboarding_first_top".localized, "onboarding_second_top".localized]
    
    var pageNumber : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Analytics.logEvent("open_onboarding", parameters: [:])
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        setupLabel()

        nextButton.layer.borderWidth = 3
        nextButton.layer.borderColor = #colorLiteral(red: 2.151550916e-06, green: 0.6969773173, blue: 0.371265173, alpha: 1)
        nextButton.layer.cornerRadius = 23
        nextButton.layer.shadowColor = #colorLiteral(red: 2.151550916e-06, green: 0.6969773173, blue: 0.371265173, alpha: 1)
        nextButton.layer.shadowOpacity = 1
        nextButton.layer.shadowOffset = .zero
        nextButton.layer.shadowRadius = 10
    }
    
    @IBAction func nextButtonTapped(_ sender: Any) {
        if pageNumber == 1{
            self.performSegue(withIdentifier: "instrument", sender: nil)
        }else{
            collectionView.scrollToItem(at: IndexPath(item: pageNumber + 1, section: 0), at: .right, animated: true)
            pageNumber += 1
            setupLabel()
        }
        
    }
    
    func setupLabel(){
        //topLbl.text = topArray[pageNumber]
        //bottomLbl.text = bottomArray[pageNumber]
    }
    
    
}

extension OnboardingViewController : UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imgArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "onboardingCell", for: indexPath) as! OnboardingCollectionViewCell
        let size = collectionView.frame.size
        cell.imgView.frame = CGRect(x: collectionView.frame.minX, y: collectionView.frame.minY, width: size.width, height: size.height)
        cell.imgView.image = imgArray[indexPath.row]
        cell.topLbl.frame = CGRect(x: 50, y: 60, width: size.width - 100, height: 150)
        cell.topLbl.text = topArray[indexPath.row]
        cell.bottomLbl.frame = CGRect(x: 40, y: size.height - 80, width: size.width - 80, height: 50)
        cell.bottomLbl.text = bottomArray[indexPath.row]
        cell.sendSubviewToBack(cell.imgView)
        
        return cell
    }
    
    
}

extension OnboardingViewController : UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = collectionView.frame.size
        return CGSize(width: (size.width), height: size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}


