//
//  SplashViewController.swift
//  Guitar Tuner
//
//  Created by Dev on 3.12.2020.
//

import UIKit
import FirebaseAuth
import FirebaseRemoteConfig
import FirebaseAnalytics
import Lottie
import Purchases

class SplashViewController: UIViewController {
    
    var remoteConfig : RemoteConfig!
    
    @IBOutlet weak var lottieView: AnimationView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lottieView.contentMode = .scaleToFill
        self.lottieView.animation = Animation.named("loader")
        self.lottieView.loopMode = .loop
        self.lottieView.play()
        
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let second = 1.5
        DispatchQueue.main.asyncAfter(deadline: .now() + second) {
            self.checkAuth()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if (UserDefaults.standard.bool(forKey: "isPremium")){
        checkIAPValidation()
        }
    }
    
    func checkAuth(){
        let cUser = Auth.auth().currentUser
            if(cUser != nil){
                //Kullanici id si varsa. hatta app silindiyse bile bu kisma duser
                 
                self.checkRemoteConfig()
            }else{
                //kullanici ilk defa giriyorsa anonim hesap olusturuyouz.
                Auth.auth().signInAnonymously { (result, err) in
                    
                    self.checkRemoteConfig()
                }
            }
    }
    
    func checkIAPValidation(){
        //Bu bolumde premium kontrolu tekrar yapiliyor. Kullanici telini hackleyip kendini premium gostermesin diye revenuecat e  kullanicinin aktif olup olmadigi soruluyor. Yani fis kontrolu yapiliyor.
        
        Purchases.shared.purchaserInfo { [self] (purchaserInfo, error) in
            if let purchaserInfo = purchaserInfo{
                let activeSubs = purchaserInfo.activeSubscriptions
                if activeSubs.count > 0 {
                    UserDefaults.standard.setValue(true, forKey: "isPremium")
                }else{
                    self.handleExpire()
                }
            }else{
                self.checkIAPValidation()
            }
        }
    }
    
    func handleExpire(){
        UserDefaults.standard.setValue(false, forKey: "isPremium")
        let alert = UIAlertController(title: "Subscription Expired", message: "Your subscription has expired or canceled.Please renew your subscription in order to continue . If you have any questions please contact us", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (aa) in
            //self.performSegue(withIdentifier: "homepageVC", sender: nil)
            self.start()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func checkRemoteConfig(){
        //analyticse tum bilgileri property olarak ekliyoruz.
        let cUser = Auth.auth().currentUser!
        let locale = Locale.current
        
        Analytics.setUserProperty(locale.regionCode ?? "", forName: "country")
        Analytics.setUserProperty(locale.languageCode ?? "", forName: "languagePreference")
        Analytics.setUserProperty(UIDevice.modelName, forName: "phoneType")
        Analytics.setUserProperty(cUser.uid, forName: "userId")
        
        //Firebase dashboardundan parametreler tanimlanir ve gelen parametreye gore ap icerisindeki gorunum ve davranislar update yapilmadan degistirilebilir.
        //Test etmek icin app sil tekrar yukle
        //Atakan'a ihtiyac var mi sor!
        
        self.remoteConfig = RemoteConfig.remoteConfig()
        self.remoteConfig.fetch { (status, error) in
            if status == .success{
                print("Config fetched!")
                
                //active islemi yapilmali
                self.remoteConfig.activate { (bool, err) in
                    if let err = err{
                        print(err.localizedDescription)
                    }
                }
                //instance
                /*
                let locale = Locale.current.regionCode ?? ""
                clH = self.remoteConfig[("close_" + locale.lowercased())].boolValue
                */
            }
            self.start()
        }
    }
    
    func start(){
        
        let counter = UserDefaults.standard.integer(forKey: "loginCounter")
        if counter == 0{
            self.performSegue(withIdentifier: "onboarding", sender: nil)
            UserDefaults.standard.setValue(1, forKey: "loginCounter")
        }else{
            self.performSegue(withIdentifier: "main", sender: nil)
            UserDefaults.standard.set(counter + 1, forKey: "loginCounter")
        }
    }
}
