//
//  PermissionViewController.swift
//  Guitar Tuner
//
//  Created by Dev on 5.12.2020.
//

import UIKit
import AVFoundation

class PermissionViewController: UIViewController {

    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var topLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        acceptButton.layer.borderWidth = 3
        acceptButton.layer.borderColor = #colorLiteral(red: 2.151550916e-06, green: 0.6969773173, blue: 0.371265173, alpha: 1)
        acceptButton.layer.cornerRadius = 23
        acceptButton.layer.shadowColor = #colorLiteral(red: 2.151550916e-06, green: 0.6969773173, blue: 0.371265173, alpha: 1)
        acceptButton.layer.shadowOpacity = 1
        acceptButton.layer.shadowOffset = .zero
        acceptButton.layer.shadowRadius = 10

        topLbl.text = "mic_access".localized
        descLbl.text = "mic_desc".localized
        acceptButton.setTitle("accept".localized, for: .normal)
        
    }
    
    @IBAction func acceptButtonTapped(_ sender: Any) {
        switch AVAudioSession.sharedInstance().recordPermission {
        case AVAudioSessionRecordPermission.granted:
            print("Permission granted")
        case AVAudioSessionRecordPermission.denied:
            print("Pemission denied")
        case AVAudioSessionRecordPermission.undetermined:
            print("Request permission here")
            AVAudioSession.sharedInstance().requestRecordPermission({ (granted) in
                DispatchQueue.main.async {
                    self.performSegue(withIdentifier: "tunerVc", sender: nil)
                }
            })
        }
    }
}
