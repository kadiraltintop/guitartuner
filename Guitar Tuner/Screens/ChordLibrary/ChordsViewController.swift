//
//  ChordsViewController.swift
//  Guitar Tuner
//
//  Created by Dev on 4.12.2020.
//

import UIKit
import AVFoundation
import GoogleMobileAds

class ChordsViewController: UIViewController, GADRewardedAdDelegate {
    
    
    
    @IBOutlet weak var topLbl: UILabel!
    @IBOutlet weak var bemolButton: UIButton!
    @IBOutlet weak var chordImg: UIImageView!
    @IBOutlet weak var notesCollectionView: UICollectionView!
    @IBOutlet weak var chordsCollectionView: UICollectionView!
    let premiumView = UIView()
    let lockLbl = UILabel()
    
    var rewardedAd : GADRewardedAd!
    
    var test : AVAudioPlayer!
    var completeVideo = false
    
    
    var selectedInstrument = 0
    var selectedNote = "C"
    var selectedChord = 0
    var chordImgArray : [String] = []
    var bemolType = false
    var selectedNoteIndex = 0
    var isPremium = false
    
    var noteArraySharp = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"]
    var noteArrayFlat = ["C", "Db", "D", "Eb", "E", "F", "Gb", "G", "Ab", "A", "Bb", "B"]
    var chordTypeArray = ["C","Cm", "C6", "C7", "C9", "Cm6", "Cm7", "Cdim", "C+", "Csus"]
    
    
    
    var unloackedNotesIndex = [0]
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        notesCollectionView.delegate = self
        notesCollectionView.dataSource = self
        chordsCollectionView.delegate = self
        chordsCollectionView.dataSource = self
        premiumView.isHidden = true
        topLbl.text = "chord_library".localized
        NotificationCenter.default.addObserver(self, selector: #selector(premiumUser), name: .premiumNotification, object: nil)
        rewardedAd = loadRewarded()
        
        
        if let time = UserDefaults.standard.object(forKey: "LastShowVideo") as? Date{
            let timeInterval = Date().timeIntervalSince(time)
            let timeInt = Int(timeInterval)
            if timeInt/86400 > 1{
                
            }else{
                let array = UserDefaults.standard.object(forKey: "UnlockedIndexArray") as? [Int] ?? [Int]()
                unloackedNotesIndex = array
                unloackedNotesIndex.append(0)
            }
        }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(
                AVAudioSession.Category.playAndRecord,
                options: [.defaultToSpeaker])
                //success = true
            } catch _ {
        }
        
    
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        lockView()
        AppCache.sharedInstance.selectedIndex = 0
        isPremium = UserDefaults.standard.bool(forKey: "isPremium")
    }
    
    @objc func premiumUser(){
        isPremium = true
        premiumView.isHidden = true
    }
    
    func convertChordName(note : String) -> [String]{
        return ["\(note)", "\(note)m", "\(note)6", "\(note)7", "\(note)9", "\(note)m6", "\(note)m7", "\(note)dim", "\(note)+", "\(note)sus"]
    }
    
    
    @IBAction func playTapped(_ sender: Any) {
        
        
        let sound = setPlaySound(instrument: selectedInstrument)
        
        sound.play()
        
        
        
    }
    
    @IBAction func bemolButtonTapped(_ sender: Any) {
        
        if bemolType{
            bemolType = false
            bemolButton.setImage(UIImage(named: "bemol"), for: .normal)
            notesCollectionView.reloadData()
            chordsCollectionView.reloadData()
        }else{
            bemolType = true
            bemolButton.setImage(UIImage(named: "diyez"), for: .normal)
            notesCollectionView.reloadData()
            chordsCollectionView.reloadData()
        }
    }
    
    func lockView(){
       
        premiumView.frame = CGRect(x: 0, y: 70, width: view.frame.size.width, height: (tabBarController?.tabBar.frame.minY)! - 130)
        premiumView.backgroundColor = #colorLiteral(red: 0.1811859608, green: 0.1763214469, blue: 0.1763671339, alpha: 1)
        view.addSubview(premiumView)
       
        let videoBtn = UIButton(frame: CGRect(x: 30, y: premiumView.frame.size.height - 79, width: view.frame.size.width - 60, height: 54))
        videoBtn.layer.borderWidth = 3
        videoBtn.layer.borderColor = #colorLiteral(red: 2.151550916e-06, green: 0.6969773173, blue: 0.371265173, alpha: 1)
        videoBtn.layer.cornerRadius = 23
        videoBtn.layer.shadowColor = #colorLiteral(red: 2.151550916e-06, green: 0.6969773173, blue: 0.371265173, alpha: 1)
        videoBtn.layer.shadowOpacity = 1
        videoBtn.layer.shadowOffset = .zero
        videoBtn.layer.shadowRadius = 10
        videoBtn.setTitle("watch_video".localized, for: .normal)
        videoBtn.setTitleColor(.white, for: .normal)
        videoBtn.titleLabel?.font = UIFont(name: "Avenir-Black", size: 20)
        videoBtn.addTarget(self, action: #selector(showVideoTapped), for: .touchUpInside)
        premiumView.addSubview(videoBtn)
        
        let premiumBtn = UIButton(frame: CGRect(x: 30, y: premiumView.frame.size.height - 155, width: view.frame.size.width - 60, height: 54))
        premiumBtn.backgroundColor = #colorLiteral(red: 2.151550916e-06, green: 0.6969773173, blue: 0.371265173, alpha: 1)
        premiumBtn.layer.cornerRadius = 23
        premiumBtn.layer.shadowColor = #colorLiteral(red: 2.151550916e-06, green: 0.6969773173, blue: 0.371265173, alpha: 1)
        premiumBtn.layer.shadowOpacity = 1
        premiumBtn.layer.shadowOffset = .zero
        premiumBtn.layer.shadowRadius = 10
        premiumBtn.setTitle("premium_btn".localized, for: .normal)
        premiumBtn.setTitleColor(.white, for: .normal)
        premiumBtn.titleLabel?.font = UIFont(name: "Avenir-Black", size: 20)
        premiumBtn.addTarget(self, action: #selector(premiumTapped), for: .touchUpInside)
        premiumView.addSubview(premiumBtn)
        
        let lockImg = UIImageView(frame: CGRect(x: (premiumView.frame.size.width - 133)/2, y: 78, width: 133, height: 164))
        lockImg.image = UIImage(named: "lockImage")
        premiumView.addSubview(lockImg)
        
        lockLbl.frame = CGRect(x: 100, y: lockImg.frame.maxY + 30, width: premiumView.frame.size.width - 200, height: 25)
        lockLbl.font = UIFont(name: "Avenir-Black", size: 25)
        lockLbl.text = "\(selectedNote)" + " " + "lock".localized
        lockLbl.textColor = .white
        lockLbl.textAlignment = .center
        premiumView.addSubview(lockLbl)
    }
    
    @objc func premiumTapped(){
        self.performSegue(withIdentifier: "premium", sender: nil)
    }
    
    @objc func showVideoTapped(){
        
        if rewardedAd.isReady{
            rewardedAd.present(fromRootViewController: self, delegate: self)
        }else{
            print("hazir degil")
        }
    }
    
    func rewardedAd(_ rewardedAd: GADRewardedAd, userDidEarn reward: GADAdReward) {
        print("odul kazanildi")
        completeVideo = true
        premiumView.isHidden = true
        unloackedNotesIndex.append(selectedNoteIndex)
        UserDefaults.standard.set(unloackedNotesIndex, forKey: "UnlockedIndexArray")
        UserDefaults.standard.setValue(Date(), forKey: "LastShowVideo")
        
    }
    
    func rewardedAd(_ rewardedAd: GADRewardedAd, didFailToPresentWithError error: Error) {
        //hata olustugunda
        print(error.localizedDescription)
        
    }
    
    func rewardedAdDidPresent(_ rewardedAd: GADRewardedAd) {
        //kullaniciya gosterilmeye basladigi anda
        print("Test")
        
    }
    
    func rewardedAdDidDismiss(_ rewardedAd: GADRewardedAd) {
        //carpiya basildiginda
        if completeVideo{
            premiumView.isHidden = true
        }else{
            premiumView.isHidden = false
        }
        notesCollectionView.reloadData()
        chordsCollectionView.reloadData()
        self.rewardedAd = loadRewarded()
        completeVideo = false
    }
    
    func loadRewarded() -> GADRewardedAd{
        let rewardedAd = GADRewardedAd(adUnitID: "/6499/example/rewarded-video")
        rewardedAd.load(GADRequest())
        return rewardedAd
    }
    
}

extension ChordsViewController : UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == notesCollectionView{
            return noteArrayFlat.count
        }else if collectionView == chordsCollectionView{
            return chordTypeArray.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let noteCell = notesCollectionView.dequeueReusableCell(withReuseIdentifier: "noteCell", for: indexPath) as! NotesCollectionViewCell
        let chordCell = chordsCollectionView.dequeueReusableCell(withReuseIdentifier: "chordCell", for: indexPath) as! ChordTypeCollectionViewCell
        
        if collectionView == notesCollectionView{
            noteCell.backView.frame = CGRect(x: 0, y: 0, width: noteCell.frame.size.width, height: noteCell.frame.size.height)
            noteCell.backView.layer.borderWidth = 1
            noteCell.backView.layer.borderColor = #colorLiteral(red: 0.29461658, green: 0.2946640551, blue: 0.2946062088, alpha: 1)
            if indexPath.row == selectedNoteIndex{
                noteCell.notesLbl.textColor = #colorLiteral(red: 0.007843137255, green: 0.6980392157, blue: 0.3725490196, alpha: 1)
            }else{
                noteCell.notesLbl.textColor = .white
            }
            
            if bemolType{
                noteCell.notesLbl.text = noteArrayFlat[indexPath.row]
            }else{
                noteCell.notesLbl.text = noteArraySharp[indexPath.row]
            }
            
            return noteCell
        }else{
            chordTypeArray = convertChordName(note: selectedNote)
            chordCell.chordLbl.text = chordTypeArray[indexPath.row]
            if indexPath.row == selectedChord{
                chordCell.chordLbl.textColor = #colorLiteral(red: 0.007843137255, green: 0.6980392157, blue: 0.3725490196, alpha: 1)
            }else{
                chordCell.chordLbl.textColor = .white
            }
            
            return chordCell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
        if collectionView == notesCollectionView{
            if bemolType{
                selectedNote = noteArrayFlat[indexPath.row]
            }else{
                selectedNote = noteArraySharp[indexPath.row]
            }
            selectedNoteIndex = indexPath.row
            chordTypeArray = convertChordName(note: selectedNote)
            chordsCollectionView.reloadData()
            
            chordImgArray = ChordHelper.sharedInstance.setChordImg(selectedInstrument: selectedInstrument, notes: indexPath.row)
            chordImg.image = UIImage(named: chordImgArray[selectedChord])
            notesCollectionView.reloadData()
            
            if !(isPremium){
                if unloackedNotesIndex.contains(indexPath.row){
                    premiumView.isHidden = true
                }else{
                    premiumView.isHidden = false
                    lockLbl.text = "\(selectedNote)" + " " + "lock".localized
                }
            }
            
        }else{
            self.selectedChord = indexPath.row
            chordImgArray = ChordHelper.sharedInstance.setChordImg(selectedInstrument: selectedInstrument, notes: indexPath.row)
            chordImg.image = UIImage(named: chordImgArray[selectedChord])
            chordsCollectionView.reloadData()
        }
    }
    
    
    func setPlaySound(instrument : Int) -> AVAudioPlayer{
        
        var test1 : NSURL!
        let convert = convertChordName(note: noteArraySharp[selectedNoteIndex])
        let son = convert[selectedChord]
        if instrument == 0{
             test1 = NSURL(fileURLWithPath: Bundle.main.path(forResource: "G-\(son)", ofType: "wav")!)
        
        }else{
             test1 = NSURL(fileURLWithPath: Bundle.main.path(forResource: "U-\(son)", ofType: "wav")!)
        }
        test = try? AVAudioPlayer(contentsOf: test1 as URL)
        test.prepareToPlay()
        
        
        
        return test
        
    }
}


extension ChordsViewController : UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == notesCollectionView{
            return CGSize(width: 75, height: 60)
        }else if collectionView == chordsCollectionView{
            return CGSize(width: 100, height: chordsCollectionView.frame.size.height / 11)
        }
        let size = collectionView.frame.size
        return CGSize(width: size.width, height: size.width)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView == notesCollectionView{
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }else if collectionView == chordsCollectionView{
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
       
        if collectionView == notesCollectionView{
            return 0
        }else if collectionView == chordsCollectionView{
            return 0
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == notesCollectionView{
            return 0
        }else if collectionView == chordsCollectionView{
            return 0
        }
        return 0
    }
}

struct unlockedChord {
    var index : Int
    var time = Date()
}

