//
//  NotesCollectionViewCell.swift
//  Guitar Tuner
//
//  Created by Dev on 4.12.2020.
//

import UIKit

class NotesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var notesLbl: UILabel!
}
