//
//  ChordHelper.swift
//  Guitar Tuner
//
//  Created by Dev on 12.12.2020.
//

import Foundation


class ChordHelper{
    static let sharedInstance = ChordHelper()
    
    func convertChordImgName(note : String) -> [String]{
        return ["G-\(note)", "G-\(note)m", "G-\(note)6", "G-\(note)7", "G-\(note)9", "G-\(note)m6", "G-\(note)m7", "G-\(note)maj7", "G-\(note)dim", "G-\(note)+", "G-\(note)sus"]
    }
    
    
    func setChordImg(selectedInstrument : Int, notes : Int) -> [String]{
        
        var returnedArray : [String] = []
        
        if selectedInstrument == 0{
            
            switch notes {
            case 0:
                returnedArray = convertChordImgName(note: "C")
            case 1:
                returnedArray = convertChordImgName(note: "C#")
            case 2:
                returnedArray = convertChordImgName(note: "D")
            case 3:
                returnedArray = convertChordImgName(note: "D#")
            case 4:
                returnedArray = convertChordImgName(note: "E")
            case 5:
                returnedArray = convertChordImgName(note: "F")
            case 6:
                returnedArray = convertChordImgName(note: "F#")
            case 7:
                returnedArray = convertChordImgName(note: "G")
            case 8:
                returnedArray = convertChordImgName(note: "G#")
            case 9:
                returnedArray = convertChordImgName(note: "A")
            case 10:
                returnedArray = convertChordImgName(note: "A#")
            case 11:
                returnedArray = convertChordImgName(note: "B")
            default:
                returnedArray = convertChordImgName(note: "C")
            }
           
            
            
        }
        
        return returnedArray
    }
    
}





/*
 if notes == 0 && chord == 0 {
     return"G-C"
 }
 if notes == 0 && chord == 1 {
     return"G-C"
 }
 if notes == 0 && chord == 2 {
     return"G-C"
 }
 if notes == 0 && chord == 3 {
     return"G-C"
 }
 if notes == 0 && chord == 4 {
     return"G-C"
 }
 if notes == 0 && chord == 5 {
     return"G-C"
 }
 if notes == 0 && chord == 6 {
     return"G-C"
 }
 if notes == 0 && chord == 7 {
     return"G-C"
 }
 if notes == 0 && chord == 8 {
     return"G-C"
 }
 if notes == 0 && chord == 9 {
     return"G-C"
 }
 if notes == 0 && chord == 10 {
     return"G-C"
 }
 */
