//
//  MetronomeViewController.swift
//  Guitar Tuner
//
//  Created by Dev on 28.11.2020.
//

import UIKit
import KnobGestureRecognizer
import AVFoundation
import FirebaseAnalytics
import GoogleMobileAds

class MetronomeViewController: UIViewController, UIGestureRecognizerDelegate {
    
    
    @IBOutlet weak var topLbl: UILabel!
    @IBOutlet weak var tapButton: UIButton!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var tempoView: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var ovalStack: UIStackView!
    @IBOutlet weak var o1: UIImageView!
    @IBOutlet weak var o2: UIImageView!
    @IBOutlet weak var o3: UIImageView!
    @IBOutlet weak var o4: UIImageView!
    @IBOutlet weak var o5: UIImageView!
    @IBOutlet weak var o6: UIImageView!
    @IBOutlet weak var o7: UIImageView!
    @IBOutlet weak var o8: UIImageView!
    @IBOutlet weak var o9: UIImageView!
    @IBOutlet weak var soundButton: UIButton!
    @IBOutlet weak var lblTempo: UILabel!
    @IBOutlet weak var hizTerimiLabel: UILabel!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var rhythmTypeButton: UIButton!
    @IBOutlet weak var lock1: UIImageView!
    @IBOutlet weak var lock2: UIImageView!
    @IBOutlet weak var lock3: UIImageView!
    
    var btn1 = UIButton()
    var btn2 = UIButton()
    var btn3 = UIButton()
    var btn4 = UIButton()
    var btn5 = UIButton()
    var btn6 = UIButton()
    var btn7 = UIButton()
    var btn8 = UIButton()
    var btn9 = UIButton()
    
    var progressBar = CircularProgressBar()
    var rhythmTypeView = UIView()
    var fluView = UIView()
    var dateArray : [Date] = []
    var dateArray2 : [Date] = []
    var timer: Timer!
    var metronomeIsOn = false
    var metronomSoundChange = 1
    var metronomeSoundPlayer: AVAudioPlayer!
    var metronomeAccentPlayer: AVAudioPlayer!
    var metronomeTahtaPlayer: AVAudioPlayer!
    var metronomeClickPlayer: AVAudioPlayer!
    var type11Sound : AVAudioPlayer!
    var type12Sound : AVAudioPlayer!
    var type21Sound : AVAudioPlayer!
    var type22Sound : AVAudioPlayer!
    var type31Sound : AVAudioPlayer!
    var type32Sound : AVAudioPlayer!
    var type41Sound : AVAudioPlayer!
    var type42Sound : AVAudioPlayer!
    var tapSound : AVAudioPlayer!
    
    var upper: Int = 4
    var lower : Int = 4
    var maxBpmValue: Int = 240
    var minBpmValue: Int = 30
    var radiansCount: CGFloat = 70
    var stopMetronomCount = 0
    
    var interstitial : GADInterstitial!
    
    var isPremium = false
    var isLock = false
    
    var bpmValue: Int = 60 {
        didSet {
            switch bpmValue {
            case let x where x < minBpmValue:
                bpmValue = minBpmValue
            case let x where x > maxBpmValue:
                bpmValue = maxBpmValue
            default:
                break
            }
            // label update edilecek
            lblTempo.text = String(bpmValue)
            setProgress()
            restartMetronome()
        }
    }
    
    var bearing: CGFloat = 0.0
    var oldBearing: CGFloat = 0.0
   
    var count: Int = 0{
        didSet {
            print(count)
            
            
            if count == 0 {
               // timeSignature.text = String(upper)
                
            } else {
               // timeSignature.text = String(count)
                setCountImg(count: count)
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        do {
            try AVAudioSession.sharedInstance().setCategory(
                AVAudioSession.Category.playAndRecord,
                options: [.defaultToSpeaker])
                //success = true
            } catch _ {
        }
        
       
        let metronomeSoundURL = NSURL(fileURLWithPath: Bundle.main.path(forResource: "normal", ofType: "wav")!)
        let metronomeTahtaURL = NSURL(fileURLWithPath: Bundle.main.path(forResource: "tahta", ofType: "wav")!)
        let metronomeClickURL = NSURL(fileURLWithPath: Bundle.main.path(forResource: "click", ofType: "wav")!)
        let type11URL = NSURL(fileURLWithPath: Bundle.main.path(forResource: "type11", ofType: "wav")!)
        let type12URL = NSURL(fileURLWithPath: Bundle.main.path(forResource: "type12", ofType: "wav")!)
        let type21URL = NSURL(fileURLWithPath: Bundle.main.path(forResource: "type21", ofType: "wav")!)
        let type22URL = NSURL(fileURLWithPath: Bundle.main.path(forResource: "type22", ofType: "wav")!)
        let type31URL = NSURL(fileURLWithPath: Bundle.main.path(forResource: "type31", ofType: "wav")!)
        let type32URL = NSURL(fileURLWithPath: Bundle.main.path(forResource: "type32", ofType: "wav")!)
        let type41URL = NSURL(fileURLWithPath: Bundle.main.path(forResource: "type41", ofType: "wav")!)
        let type42URL = NSURL(fileURLWithPath: Bundle.main.path(forResource: "type42", ofType: "wav")!)
        let tapURL = NSURL(fileURLWithPath: Bundle.main.path(forResource: "tapSound", ofType: "wav")!)
        type11Sound = try? AVAudioPlayer(contentsOf: type11URL as URL)
        type12Sound = try? AVAudioPlayer(contentsOf: type12URL as URL)
        type21Sound = try? AVAudioPlayer(contentsOf: type21URL as URL)
        type22Sound = try? AVAudioPlayer(contentsOf: type22URL as URL)
        type31Sound = try? AVAudioPlayer(contentsOf: type31URL as URL)
        type32Sound = try? AVAudioPlayer(contentsOf: type32URL as URL)
        type41Sound = try? AVAudioPlayer(contentsOf: type41URL as URL)
        type42Sound = try? AVAudioPlayer(contentsOf: type42URL as URL)
        tapSound = try? AVAudioPlayer(contentsOf: tapURL as URL)
        type11Sound.prepareToPlay()
        type12Sound.prepareToPlay()
        type21Sound.prepareToPlay()
        type22Sound.prepareToPlay()
        type31Sound.prepareToPlay()
        type32Sound.prepareToPlay()
        type41Sound.prepareToPlay()
        type42Sound.prepareToPlay()
        tapSound.prepareToPlay()
        metronomeSoundPlayer = try? AVAudioPlayer(contentsOf: metronomeSoundURL as URL)
        metronomeTahtaPlayer = try? AVAudioPlayer(contentsOf: metronomeTahtaURL as URL)
        metronomeClickPlayer = try? AVAudioPlayer(contentsOf: metronomeClickURL as URL)
        metronomeSoundPlayer.prepareToPlay()
        metronomeTahtaPlayer.prepareToPlay()
        metronomeClickPlayer.prepareToPlay()
        
        setButton()
        setProgressView()
        
        rhythmTypeView.alpha = 0
        ovalStack.isHidden = true
        
        imgView.isUserInteractionEnabled = true
        let gesture = KnobGestureRecognizer(target: self, action: #selector(rotationAction(_:)), to: imgView)
        gesture.delegate = self
        imgView.addGestureRecognizer(gesture)
        calculateRhythmOvalImg(upper: 4, lower: 4)
        
        lock1.isHidden = true
        lock2.isHidden = true
        lock3.isHidden = true
        
        interstitial = loadInterstitial()
        
        NotificationCenter.default.addObserver(self, selector: #selector(premiumUser), name: .premiumNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppCache.sharedInstance.selectedIndex = 2
        isPremium = UserDefaults.standard.bool(forKey: "isPremium")
        print(isPremium)
    }
    
    @objc func premiumUser(){
        isPremium = true
        lockFunctions()
    }
    
    
    func setProgressView(){
        progressBar.frame = CGRect(x: 0, y: 0, width: tempoView.frame.size.width, height: tempoView.frame.size.height)
        tempoView.addSubview(progressBar)
        imgView.image = UIImage(named: "Pgrs – 1-1 kopyası")
        tempoView.addSubview(imgView)
        tempoView.bringSubviewToFront(btnPlay)
        rhythmTypeButton.setTitle("4/4", for: .normal)
    }
    
    func setButton(){
        topLbl.text = "metronome".localized
        tapButton.layer.cornerRadius = 5
        tapButton.layer.borderWidth = 3
        tapButton.layer.borderColor = #colorLiteral(red: 2.151550916e-06, green: 0.6969773173, blue: 0.371265173, alpha: 1)
        
        rhythmTypeButton.layer.cornerRadius = 5
        rhythmTypeButton.layer.borderWidth = 3
        rhythmTypeButton.layer.borderColor = #colorLiteral(red: 2.151550916e-06, green: 0.6969773173, blue: 0.371265173, alpha: 1)
        rhythmTypeButton.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        soundButton.layer.cornerRadius = 5
        soundButton.layer.borderWidth = 3
        soundButton.layer.borderColor = #colorLiteral(red: 2.151550916e-06, green: 0.6969773173, blue: 0.371265173, alpha: 1)
        soundButton.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
    }
    
    func lockFunctions(){
        if !(isPremium){
            isLock = true
            tapButton.layer.borderColor = #colorLiteral(red: 0.1811859608, green: 0.1763214469, blue: 0.1763671339, alpha: 1)
            rhythmTypeButton.layer.borderColor = #colorLiteral(red: 0.1811859608, green: 0.1763214469, blue: 0.1763671339, alpha: 1)
            soundButton.layer.borderColor = #colorLiteral(red: 0.1811859608, green: 0.1763214469, blue: 0.1763671339, alpha: 1)
            tapButton.setTitle("", for: .normal)
            rhythmTypeButton.setTitle("", for: .normal)
            soundButton.setImage(nil, for: .normal)
            lock1.isHidden = false
            lock2.isHidden = false
            lock3.isHidden = false
        }else{
            isLock = false
            tapButton.layer.borderColor = #colorLiteral(red: 2.151550916e-06, green: 0.6969773173, blue: 0.371265173, alpha: 1)
            rhythmTypeButton.layer.borderColor = #colorLiteral(red: 2.151550916e-06, green: 0.6969773173, blue: 0.371265173, alpha: 1)
            soundButton.layer.borderColor = #colorLiteral(red: 2.151550916e-06, green: 0.6969773173, blue: 0.371265173, alpha: 1)
            tapButton.setTitle("TAP", for: .normal)
            rhythmTypeButton.setTitle("\(upper)/\(lower)", for: .normal)
            soundButton.setImage(UIImage(named: "volume1"), for: .normal)
            lock1.isHidden = true
            lock2.isHidden = true
            lock3.isHidden = true
        }
    }
    
    func setRhythmTypeView(){
        let size = view.bounds.size
        
        rhythmTypeView.frame = CGRect(x: 0, y: 81, width: size.width, height: size.height - 81 )
        rhythmTypeView.backgroundColor = #colorLiteral(red: 0.1176293269, green: 0.1176572964, blue: 0.1176275387, alpha: 1)
        view.addSubview(rhythmTypeView)
        
        let closeButton = UIButton(frame: CGRect(x: 36, y: 12, width: 20, height: 20))
        closeButton.setBackgroundImage(UIImage(named: "close"), for: .normal)
        closeButton.addTarget(self, action: #selector(closeButtonTapped), for: .touchUpInside)
        rhythmTypeView.addSubview(closeButton)
        
        let height = (rhythmTypeView.frame.size.height - 70) / 11
        
        btn1.frame = CGRect(x: 18, y: 70, width: rhythmTypeView.frame.size.width - 36 , height: height)
        btn1.setTitle("1/4", for: .normal)
        btn1.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 20)
        btn1.tag = 1
        btn1.addTarget(self, action: #selector(rhythmBtnTapped(sender:)), for: .touchUpInside)
        rhythmTypeView.addSubview(btn1)
        
        
        
        btn2.frame = CGRect(x: 18, y: 70 + height, width: rhythmTypeView.frame.size.width - 36 , height: height)
        btn2.setTitle("2/4", for: .normal)
        btn2.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 20)
        btn2.tag = 2
        btn2.addTarget(self, action: #selector(rhythmBtnTapped(sender:)), for: .touchUpInside)
        rhythmTypeView.addSubview(btn2)
        
        btn3.frame = CGRect(x: 18, y: 70 + (2 * height), width: rhythmTypeView.frame.size.width - 36 , height: height)
        btn3.setTitle("3/4", for: .normal)
        btn3.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 20)
        btn3.tag = 3
        btn3.addTarget(self, action: #selector(rhythmBtnTapped(sender:)), for: .touchUpInside)
        rhythmTypeView.addSubview(btn3)
        
        btn4.frame = CGRect(x: 18, y: 70 + (3 * height), width: rhythmTypeView.frame.size.width - 36 , height: height)
        btn4.setTitle("4/4", for: .normal)
        btn4.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 20)
        btn4.tag = 4
        btn4.addTarget(self, action: #selector(rhythmBtnTapped(sender:)), for: .touchUpInside)
        
        rhythmTypeView.addSubview(btn4)
        
        btn5.frame = CGRect(x: 18, y: 70 + (4 * height), width: rhythmTypeView.frame.size.width - 36 , height: height)
        btn5.setTitle("5/4", for: .normal)
        btn5.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 20)
        btn5.tag = 5
        btn5.addTarget(self, action: #selector(rhythmBtnTapped(sender:)), for: .touchUpInside)
        
        rhythmTypeView.addSubview(btn5)
        
        btn6.frame = CGRect(x: 18, y: 70 + (5 * height), width: rhythmTypeView.frame.size.width - 36 , height: height)
        btn6.setTitle("6/4", for: .normal)
        btn6.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 20)
        btn6.tag = 6
        btn6.addTarget(self, action: #selector(rhythmBtnTapped(sender:)), for: .touchUpInside)
        
        rhythmTypeView.addSubview(btn6)
        
        btn7.frame = CGRect(x: 18, y: 70 + (6 * height), width: rhythmTypeView.frame.size.width - 36 , height: height)
        btn7.setTitle("3/8", for: .normal)
        btn7.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 20)
        btn7.tag = 7
        btn7.addTarget(self, action: #selector(rhythmBtnTapped(sender:)), for: .touchUpInside)
        
        rhythmTypeView.addSubview(btn7)
        
        btn8.frame = CGRect(x: 18, y: 70 + (7 * height), width: rhythmTypeView.frame.size.width - 36 , height: height)
        btn8.setTitle("6/8", for: .normal)
        btn8.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 20)
        btn8.tag = 8
        btn8.addTarget(self, action: #selector(rhythmBtnTapped(sender:)), for: .touchUpInside)
        
        rhythmTypeView.addSubview(btn8)
        
        btn9.frame = CGRect(x: 18, y: 70 + (8 * height), width: rhythmTypeView.frame.size.width - 36 , height: height)
        btn9.setTitle("9/8", for: .normal)
        btn9.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 20)
        btn9.tag = 9
        btn9.addTarget(self, action: #selector(rhythmBtnTapped(sender:)), for: .touchUpInside)
        
        rhythmTypeView.addSubview(btn9)
        
        
        
        
    }
    
    func loadInterstitial() -> GADInterstitial{
        
        let interstitial = GADInterstitial(adUnitID: "/6499/example/interstitial")
        interstitial.delegate = self
        interstitial.load(GADRequest())
        return interstitial
    }
    
    
    @objc func rhythmBtnTapped(sender : UIButton){
        switch sender.tag {
        case 1:
            upper = 1
            lower = 4
        case 2:
            upper = 2
            lower = 4
        case 3:
            upper = 3
            lower = 4
        case 4:
            upper = 4
            lower = 4
        case 5:
            upper = 5
            lower = 4
        case 6:
            upper = 6
            lower = 4
        case 7:
            upper = 3
            lower = 8
        case 8:
            upper = 6
            lower = 8
        case 9:
            upper = 9
            lower = 8
        
        default:
            upper = 4
            lower = 4
        }
        
        closeButtonTapped()
        rhythmTypeButton.setTitle("\(upper)/\(lower)", for: .normal)
        calculateRhythmOvalImg(upper: upper, lower: lower)
        
    }
    
    
    @IBAction func soundButtonTapped(_ sender: Any) {
        if !isLock{
            if metronomSoundChange == 4{
                metronomSoundChange = 1
            } else {
                metronomSoundChange += 1
            }
            if metronomeIsOn == false{
                switch metronomSoundChange {
                       case 1:
                         type11Sound.play()
                       case 2:
                         type21Sound.play()
                       case 3:
                         type31Sound.play()
                       case 4:
                         type41Sound.play()
                       
                       default:
                           break
                       }
            }
        }else{
            self.performSegue(withIdentifier: "premium", sender: nil)
        }
        
    }
    
    @IBAction func playButtonTapped(_ sender: Any) {
        
        if !(isPremium){
            let number = UserDefaults.standard.object(forKey: "loginCounter") as! Int
            
            stopMetronomCount += 1
            if stopMetronomCount % 6 == 0{
                if self.interstitial.isReady{
                    self.interstitial.present(fromRootViewController: self)
                }else{
                    print("hazir degil")
                }
            }
            
            
        }
        if metronomeIsOn {
            stopMetronome(sender: btnPlay)
        } else {
         //stopMetronome(sender: btnPlay)
         switch metronomSoundChange {
         case 1:
             type11Sound.play()
         case 2:
             type21Sound.play()
         case 3:
             type31Sound.play()
         case 4:
             type41Sound.play()
         default:
             break
         }
            
            count = 1
            startMetronome(sender: btnPlay)
        }
    }
    
    @IBAction func rhythmTypeTapped(_ sender: Any) {
        if !isLock{
            setRhythmTypeView()
            setRhythmViewBorderColor(upper: upper, lower: lower)
            metronomeIsOn = false
            timer?.invalidate()
            count = 0
            UIView.animate(withDuration: 0.5) {
                self.rhythmTypeView.alpha = 1.0
                self.fluView.alpha = 0.75
            }
        }else{
            self.performSegue(withIdentifier: "premium", sender: nil)
        }
        
    }
    
    @objc func closeButtonTapped(){
        UIView.animate(withDuration: 0.5) {
            self.rhythmTypeView.alpha = 0
            self.fluView.alpha = 0
        }
    }
    
    @IBAction func tapButtonTapped(_ sender: Any) {
        if !isLock{
            let DateNow = Date()
            dateArray.append(DateNow)
            calculateRitim()
        }else{
            self.performSegue(withIdentifier: "premium", sender: nil)
        }
        
    }
    
    
    
    
    //MARK: - Functions
    
    //kullanici 10 saniyeden fazla metronomu kullandiysa event gonderiyoruz.
    func metronomUsageTime(){
        let timeInterval = dateArray2[dateArray2.count-1] .timeIntervalSince(dateArray2[dateArray2.count-2])
        
        print(timeInterval)
        if timeInterval > 10{
            print("event gonder")
            Analytics.logEvent("use_metronom_more_than_ten_seconds", parameters: [:])
            
        }
    }
          
    @objc  func calculateRitim(){
        
        tapSound.play()
        
        if dateArray.count < 2 {
            return
        }
        
        let timeInterval = dateArray[dateArray.count-1] .timeIntervalSince(dateArray[dateArray.count-2])
        
        print(timeInterval)
        
        bpmValue = Int(60 / timeInterval)
        
    }
    
    func setProgress(){
         
        if bpmValue == 30 {
            progressBar.setProgress(to: Double(0.100), withAnimation: true)
        }else if bpmValue == 240 {
            progressBar.setProgress(to: Double(1), withAnimation: true)
        }else{
            progressBar.setProgress(to: Double(Double(bpmValue) * 0.00396) , withAnimation: true)
        }
        hizTerimiDegistir(speed: bpmValue)
    }
    
    func stopMetronome(sender: UIButton) {
        
        
        let DateNow = Date()
        dateArray2.append(DateNow)
        metronomUsageTime()
        
        btnPlay.setImage(UIImage(named: "playMetronom"), for: .normal)

        metronomeIsOn = false
        timer?.invalidate()
        count = 0
        setCountImg(count: 0)
        
       }
       
    func startMetronome(sender: UIButton) {
        
        let DateNow = Date()
        dateArray2.append(DateNow)
        
        if !isPremium{
            lockFunctions()
        }else{
            
        }
      
        btnPlay.setImage(UIImage(named: "stopMetronom"), for: .normal)
        
        metronomeIsOn = true
        let metronomeTimeInterval:TimeInterval = (Double(60)) / Double(bpmValue)
        
        timer = Timer.scheduledTimer(timeInterval: metronomeTimeInterval, target: self, selector: #selector(playMetronomeSound), userInfo: nil, repeats: true)
        
        RunLoop.current.add(timer, forMode: .common)
        
    }
    
    @objc func playMetronomeSound() {
        
        if upper == 1 && lower == 4{
            switch metronomSoundChange {
            case 1:
                type12Sound.play()
            case 2:
                type22Sound.play()
            case 3:
                type32Sound.play()
            case 4:
                type42Sound.play()
            default:
                break
            }
        }else{
            count += 1
            if count == 1 {
              switch metronomSoundChange {
              case 1:
                  type11Sound.play()
              case 2:
                  type21Sound.play()
              case 3:
                  type31Sound.play()
              case 4:
                  type41Sound.play()
              default:
                  break
              }
              
              
            } else {
              switch metronomSoundChange {
              case 1:
                  type12Sound.play()
                  if count == upper {
                      count = 0
                  }
                  
              case 2:
                  type22Sound.play()
                  if count == upper {
                      count = 0
                  }
              case 3:
                  type32Sound.play()
                  if count == upper {
                      count = 0
                  }
              case 4:
                  type42Sound.play()
                  if count == upper {
                      count = 0
                  }
              default:
                  break
              }
            }
        }
    }
    
    func restartMetronome() {
        if metronomeIsOn {
            stopMetronome(sender: btnPlay)
            startMetronome(sender: btnPlay)
        }
    }
     
    
    func hizTerimiDegistir(speed: Int){
        
        if (speed <= 50) {
                hizTerimiLabel.text = "Grave"
        } else if (speed > 50 && speed < 55) {
                hizTerimiLabel.text = "Largo"
        } else if (speed > 55 && speed < 60) {
                hizTerimiLabel.text = "Larghetto"
        } else if (speed >= 60 && speed < 70) {
                hizTerimiLabel.text = "Adagio"
        } else if (speed > 70 && speed < 85) {
                hizTerimiLabel.text = "Andante"
        } else if (speed > 85 && speed < 100) {
                hizTerimiLabel.text = "Moderato"
        } else if (speed > 100 && speed < 115) {
                hizTerimiLabel.text = "Allegretto"
        } else if (speed > 115 && speed < 140) {
                hizTerimiLabel.text = "Allegro"
        } else if (speed > 140 && speed < 150) {
                hizTerimiLabel.text = "Vivace"
        } else if (speed > 150 && speed < 170) {
                hizTerimiLabel.text = "Presto"
        } else if (speed > 150) {
                hizTerimiLabel.text = "Prestissimo"
        }
    }
    
    

    //MARK: - RecognizerFunctions
    
    @objc func rotationAction(_ sender: KnobGestureRecognizer) {
    print("gesture calisiyor")
        switch sender.state {
        case .began:
            imgView.isHighlighted = true
            bearing = 0.0
            gestureHandler(sender)
        case .changed:
            gestureHandler(sender)
        case .ended:
             imgView.isHighlighted = false
        default: break
        }
    }
    
    fileprivate func gestureHandler(_ sender: KnobGestureRecognizer) {
        
        bearing +=  radiansCount/2 * sender.rotation / .pi
        
        if round(bearing) > oldBearing {
            bpmValue += 1
        } else if round(bearing) < oldBearing {
            bpmValue -= 1
        }

        oldBearing = round(bearing)
        
        let viewTransform = imgView.transform
        let newTransform = viewTransform.rotated(by: sender.rotation)
        imgView.transform = newTransform
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        imgView.isHighlighted = true
        
        return true
    }
    
    
    func calculateRhythmOvalImg(upper : Int, lower : Int){
        
        if upper == 1 && lower == 4{
            ovalStack.isHidden = true
        }
        if upper == 2 && lower == 4{
            ovalStack.isHidden = false
            o1.isHidden = false
            o2.isHidden = false
            o3.isHidden = true
            o4.isHidden = true
            o5.isHidden = true
            o6.isHidden = true
            o7.isHidden = true
            o8.isHidden = true
            o9.isHidden = true
           
        }
        
        if upper == 3 && lower == 4{
            ovalStack.isHidden = false
            o1.isHidden = false
            o2.isHidden = false
            o3.isHidden = false
            o4.isHidden = true
            o5.isHidden = true
            o6.isHidden = true
            o7.isHidden = true
            o8.isHidden = true
            o9.isHidden = true
           
        }
        if upper == 4 && lower == 4{
            ovalStack.isHidden = false
            o1.isHidden = false
            o2.isHidden = false
            o3.isHidden = false
            o4.isHidden = false
            o5.isHidden = true
            o6.isHidden = true
            o7.isHidden = true
            o8.isHidden = true
            o9.isHidden = true
           
        }
        
        if upper == 5 && lower == 4{
            ovalStack.isHidden = false
            o1.isHidden = false
            o2.isHidden = false
            o3.isHidden = false
            o4.isHidden = false
            o5.isHidden = false
            o6.isHidden = true
            o7.isHidden = true
            o8.isHidden = true
            o9.isHidden = true
            
        }
        
        if upper == 6 && lower == 4{
            ovalStack.isHidden = false
            o1.isHidden = false
            o2.isHidden = false
            o3.isHidden = false
            o4.isHidden = false
            o5.isHidden = false
            o6.isHidden = false
            o7.isHidden = true
            o8.isHidden = true
            o9.isHidden = true
           
        }
        
        if upper == 3 && lower == 8{
            ovalStack.isHidden = false
            o1.isHidden = false
            o2.isHidden = false
            o3.isHidden = false
            o4.isHidden = true
            o5.isHidden = true
            o6.isHidden = true
            o7.isHidden = true
            o8.isHidden = true
            o9.isHidden = true
            
        }
        
        if upper == 6 && lower == 8{
            ovalStack.isHidden = false
            o1.isHidden = false
            o2.isHidden = false
            o3.isHidden = false
            o4.isHidden = false
            o5.isHidden = false
            o6.isHidden = false
            o7.isHidden = true
            o8.isHidden = true
            o9.isHidden = true
            
        }
        
        if upper == 9 && lower == 8{
            ovalStack.isHidden = false
            o1.isHidden = false
            o2.isHidden = false
            o3.isHidden = false
            o4.isHidden = false
            o5.isHidden = false
            o6.isHidden = false
            o7.isHidden = false
            o8.isHidden = false
            o9.isHidden = false
          
        }
        
    }
    
    
    //ritim sayilirken kucuk image degisitiriliyor
    func setCountImg(count : Int){
        switch count{
        case 0:
            o1.image = UIImage(named: "o3")
            o2.image = UIImage(named: "o3")
            o3.image = UIImage(named: "o3")
            o4.image = UIImage(named: "o3")
            o5.image = UIImage(named: "o3")
            o6.image = UIImage(named: "o3")
            o7.image = UIImage(named: "o3")
            o8.image = UIImage(named: "o3")
            o9.image = UIImage(named: "o3")
            
        case 1:
            o1.image = UIImage(named: "o1")
            o2.image = UIImage(named: "o3")
            o3.image = UIImage(named: "o3")
            o4.image = UIImage(named: "o3")
            o5.image = UIImage(named: "o3")
            o6.image = UIImage(named: "o3")
            o7.image = UIImage(named: "o3")
            o8.image = UIImage(named: "o3")
            o9.image = UIImage(named: "o3")
          
        case 2:
            o1.image = UIImage(named: "o3")
            o2.image = UIImage(named: "o1")
            o3.image = UIImage(named: "o3")
            o4.image = UIImage(named: "o3")
            o5.image = UIImage(named: "o3")
            o6.image = UIImage(named: "o3")
            o7.image = UIImage(named: "o3")
            o8.image = UIImage(named: "o3")
            o9.image = UIImage(named: "o3")
            
        case 3:
            o1.image = UIImage(named: "o3")
            o2.image = UIImage(named: "o3")
            o3.image = UIImage(named: "o1")
            o4.image = UIImage(named: "o3")
            o5.image = UIImage(named: "o3")
            o6.image = UIImage(named: "o3")
            o7.image = UIImage(named: "o3")
            o8.image = UIImage(named: "o3")
            o9.image = UIImage(named: "o3")
            
        case 4:
            o1.image = UIImage(named: "o3")
            o2.image = UIImage(named: "o3")
            o3.image = UIImage(named: "o3")
            o4.image = UIImage(named: "o1")
            o5.image = UIImage(named: "o3")
            o6.image = UIImage(named: "o3")
            o7.image = UIImage(named: "o3")
            o8.image = UIImage(named: "o3")
            o9.image = UIImage(named: "o3")
            
        case 5:
            o1.image = UIImage(named: "o3")
            o2.image = UIImage(named: "o3")
            o3.image = UIImage(named: "o3")
            o4.image = UIImage(named: "o3")
            o5.image = UIImage(named: "o1")
            o6.image = UIImage(named: "o3")
            o7.image = UIImage(named: "o3")
            o8.image = UIImage(named: "o3")
            o9.image = UIImage(named: "o3")
           
        case 6:
            o1.image = UIImage(named: "o3")
            o2.image = UIImage(named: "o3")
            o3.image = UIImage(named: "o3")
            o4.image = UIImage(named: "o3")
            o5.image = UIImage(named: "o3")
            o6.image = UIImage(named: "o1")
            o7.image = UIImage(named: "o3")
            o8.image = UIImage(named: "o3")
            o9.image = UIImage(named: "o3")
           
        case 7:
            o1.image = UIImage(named: "o3")
            o2.image = UIImage(named: "o3")
            o3.image = UIImage(named: "o3")
            o4.image = UIImage(named: "o3")
            o5.image = UIImage(named: "o3")
            o6.image = UIImage(named: "o3")
            o7.image = UIImage(named: "o1")
            o8.image = UIImage(named: "o3")
            o9.image = UIImage(named: "o3")
            
        case 8:
            o1.image = UIImage(named: "o3")
            o2.image = UIImage(named: "o3")
            o3.image = UIImage(named: "o3")
            o4.image = UIImage(named: "o3")
            o5.image = UIImage(named: "o3")
            o6.image = UIImage(named: "o3")
            o7.image = UIImage(named: "o3")
            o8.image = UIImage(named: "o1")
            o9.image = UIImage(named: "o3")
            
        case 9:
            o1.image = UIImage(named: "o3")
            o2.image = UIImage(named: "o3")
            o3.image = UIImage(named: "o3")
            o4.image = UIImage(named: "o3")
            o5.image = UIImage(named: "o3")
            o6.image = UIImage(named: "o3")
            o7.image = UIImage(named: "o3")
            o8.image = UIImage(named: "o3")
            o9.image = UIImage(named: "o1")
            
        case 10:
            o1.image = UIImage(named: "o3")
            o2.image = UIImage(named: "o3")
            o3.image = UIImage(named: "o3")
            o4.image = UIImage(named: "o3")
            o5.image = UIImage(named: "o3")
            o6.image = UIImage(named: "o3")
            o7.image = UIImage(named: "o3")
            o8.image = UIImage(named: "o3")
            o9.image = UIImage(named: "o3")
            
        case 11:
            o1.image = UIImage(named: "o3")
            o2.image = UIImage(named: "o3")
            o3.image = UIImage(named: "o3")
            o4.image = UIImage(named: "o3")
            o5.image = UIImage(named: "o3")
            o6.image = UIImage(named: "o3")
            o7.image = UIImage(named: "o3")
            o8.image = UIImage(named: "o3")
            o9.image = UIImage(named: "o3")
           
        case 12:
            o1.image = UIImage(named: "o3")
            o2.image = UIImage(named: "o3")
            o3.image = UIImage(named: "o3")
            o4.image = UIImage(named: "o3")
            o5.image = UIImage(named: "o3")
            o6.image = UIImage(named: "o3")
            o7.image = UIImage(named: "o3")
            o8.image = UIImage(named: "o3")
            o9.image = UIImage(named: "o3")
           
            
        default:
            o1.image = UIImage(named: "o3")
            o2.image = UIImage(named: "o3")
            o3.image = UIImage(named: "o3")
            o4.image = UIImage(named: "o3")
            o5.image = UIImage(named: "o3")
            o6.image = UIImage(named: "o3")
            o7.image = UIImage(named: "o3")
            o8.image = UIImage(named: "o3")
            o9.image = UIImage(named: "o3")
            
        }
    }
    
    func setRhythmViewBorderColor(upper : Int, lower : Int){
        if upper == 1 && lower == 4{
            btn1.layer.cornerRadius = 24
            btn1.layer.borderWidth = 3
            btn1.layer.borderColor = #colorLiteral(red: 2.151550916e-06, green: 0.6969773173, blue: 0.371265173, alpha: 1)
            btn1.setTitleColor(#colorLiteral(red: 2.151550916e-06, green: 0.6969773173, blue: 0.371265173, alpha: 1), for: .normal)
        }else{
            btn1.layer.borderWidth = 3
            btn1.layer.borderColor = #colorLiteral(red: 0.1176293269, green: 0.1176572964, blue: 0.1176275387, alpha: 1)
            btn1.setTitleColor(#colorLiteral(red: 0.2940856516, green: 0.2941381931, blue: 0.2940781415, alpha: 1), for: .normal)
        }
        if upper == 2 && lower == 4{
            btn2.layer.cornerRadius = 24
            btn2.layer.borderWidth = 3
            btn2.layer.borderColor = #colorLiteral(red: 2.151550916e-06, green: 0.6969773173, blue: 0.371265173, alpha: 1)
            btn2.setTitleColor(#colorLiteral(red: 2.151550916e-06, green: 0.6969773173, blue: 0.371265173, alpha: 1), for: .normal)
        }else{
            btn2.layer.borderWidth = 3
            btn2.layer.borderColor = #colorLiteral(red: 0.1176293269, green: 0.1176572964, blue: 0.1176275387, alpha: 1)
            btn2.setTitleColor(#colorLiteral(red: 0.2940856516, green: 0.2941381931, blue: 0.2940781415, alpha: 1), for: .normal)
        }
        if upper == 3 && lower == 4{
            btn3.layer.cornerRadius = 24
            btn3.layer.borderWidth = 3
            btn3.layer.borderColor = #colorLiteral(red: 2.151550916e-06, green: 0.6969773173, blue: 0.371265173, alpha: 1)
            btn3.setTitleColor(#colorLiteral(red: 2.151550916e-06, green: 0.6969773173, blue: 0.371265173, alpha: 1), for: .normal)
        }else{
            btn3.layer.borderWidth = 3
            btn3.layer.borderColor = #colorLiteral(red: 0.1176293269, green: 0.1176572964, blue: 0.1176275387, alpha: 1)
            btn3.setTitleColor(#colorLiteral(red: 0.2940856516, green: 0.2941381931, blue: 0.2940781415, alpha: 1), for: .normal)
        }
        if upper == 4 && lower == 4{
            btn4.layer.cornerRadius = 24
            btn4.layer.borderWidth = 3
            btn4.layer.borderColor = #colorLiteral(red: 2.151550916e-06, green: 0.6969773173, blue: 0.371265173, alpha: 1)
            btn4.setTitleColor(#colorLiteral(red: 2.151550916e-06, green: 0.6969773173, blue: 0.371265173, alpha: 1), for: .normal)
        }else{
            btn4.layer.borderWidth = 3
            btn4.layer.borderColor = #colorLiteral(red: 0.1176293269, green: 0.1176572964, blue: 0.1176275387, alpha: 1)
            btn4.setTitleColor(#colorLiteral(red: 0.2940856516, green: 0.2941381931, blue: 0.2940781415, alpha: 1), for: .normal)
        }
        if upper == 5 && lower == 4{
            btn5.layer.cornerRadius = 24
            btn5.layer.borderWidth = 3
            btn5.layer.borderColor = #colorLiteral(red: 2.151550916e-06, green: 0.6969773173, blue: 0.371265173, alpha: 1)
            btn5.setTitleColor(#colorLiteral(red: 2.151550916e-06, green: 0.6969773173, blue: 0.371265173, alpha: 1), for: .normal)
        }else{
            btn5.layer.borderWidth = 3
            btn5.layer.borderColor = #colorLiteral(red: 0.1176293269, green: 0.1176572964, blue: 0.1176275387, alpha: 1)
            btn5.setTitleColor(#colorLiteral(red: 0.2940856516, green: 0.2941381931, blue: 0.2940781415, alpha: 1), for: .normal)
        }
        if upper == 6 && lower == 4{
            btn6.layer.cornerRadius = 24
            btn6.layer.borderWidth = 3
            btn6.layer.borderColor = #colorLiteral(red: 2.151550916e-06, green: 0.6969773173, blue: 0.371265173, alpha: 1)
            btn6.setTitleColor(#colorLiteral(red: 2.151550916e-06, green: 0.6969773173, blue: 0.371265173, alpha: 1), for: .normal)
        }else{
            btn6.layer.borderWidth = 3
            btn6.layer.borderColor = #colorLiteral(red: 0.1176293269, green: 0.1176572964, blue: 0.1176275387, alpha: 1)
            btn6.setTitleColor(#colorLiteral(red: 0.2940856516, green: 0.2941381931, blue: 0.2940781415, alpha: 1), for: .normal)
        }
        if upper == 3 && lower == 8{
            btn7.layer.cornerRadius = 24
            btn7.layer.borderWidth = 3
            btn7.layer.borderColor = #colorLiteral(red: 2.151550916e-06, green: 0.6969773173, blue: 0.371265173, alpha: 1)
            btn7.setTitleColor(#colorLiteral(red: 2.151550916e-06, green: 0.6969773173, blue: 0.371265173, alpha: 1), for: .normal)
        }else{
            btn7.layer.borderWidth = 3
            btn7.layer.borderColor = #colorLiteral(red: 0.1176293269, green: 0.1176572964, blue: 0.1176275387, alpha: 1)
            btn7.setTitleColor(#colorLiteral(red: 0.2940856516, green: 0.2941381931, blue: 0.2940781415, alpha: 1), for: .normal)
        }
        if upper == 6 && lower == 8{
            btn8.layer.cornerRadius = 24
            btn8.layer.borderWidth = 3
            btn8.layer.borderColor = #colorLiteral(red: 2.151550916e-06, green: 0.6969773173, blue: 0.371265173, alpha: 1)
            btn8.setTitleColor(#colorLiteral(red: 2.151550916e-06, green: 0.6969773173, blue: 0.371265173, alpha: 1), for: .normal)
        }else{
            btn8.layer.borderWidth = 3
            btn8.layer.borderColor = #colorLiteral(red: 0.1176293269, green: 0.1176572964, blue: 0.1176275387, alpha: 1)
            btn8.setTitleColor(#colorLiteral(red: 0.2940856516, green: 0.2941381931, blue: 0.2940781415, alpha: 1), for: .normal)
        }
        if upper == 9 && lower == 8{
            btn9.layer.cornerRadius = 24
            btn9.layer.borderWidth = 3
            btn9.layer.borderColor = #colorLiteral(red: 2.151550916e-06, green: 0.6969773173, blue: 0.371265173, alpha: 1)
            btn9.setTitleColor(#colorLiteral(red: 2.151550916e-06, green: 0.6969773173, blue: 0.371265173, alpha: 1), for: .normal)
        }else{
            btn9.layer.borderWidth = 3
            btn9.layer.borderColor = #colorLiteral(red: 0.1176293269, green: 0.1176572964, blue: 0.1176275387, alpha: 1)
            btn9.setTitleColor(#colorLiteral(red: 0.2940856516, green: 0.2941381931, blue: 0.2940781415, alpha: 1), for: .normal)
        }
    }


}

extension MetronomeViewController : GADInterstitialDelegate{
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        //hata oldugunda
    }
    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
        //kullaniciya gosterildiginde
    }
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        //carpiya basildiginda
        interstitial = self.loadInterstitial()
    }
}



