//
//  TMTuner.swift
//  Guitar Tuner
//
//  Created by Dev on 29.11.2020.
//

import Foundation
import AudioKit


class TMTuner {
    
    let engine = AudioEngine()
    let mic: AudioEngine.InputNode
    let tracker: PitchTap
    let silence: Fader
    
    
    //kac saniyede bir nota sorgusu yapmasi isteniyor
    let pollingInterval = 0.3
    var delegate: TunerDelegate?
    var pollingTimer: Timer?
    
    init() {
        
        Settings.audioInputEnabled = true
        mic = engine.input!
       
        tracker = PitchTap.init(mic, handler: { (_, _) in
            
        })
        silence = Fader(tracker.input, gain: 0)
        engine.output = silence
       
    }
    
    //Islem basladiginda belirledigimiz araliklarla pollingtick func isleme alinip ciktilar verilecek.
    func start() {
        do {
            try engine.start()
            tracker.start()
        } catch let error {
            print(error.localizedDescription)
        }
        
        pollingTimer = Timer.scheduledTimer(withTimeInterval: TimeInterval(pollingInterval), repeats: true, block: {_ in self.pollingTick()})
    }
    
    func stop() {
        do {
            try engine.stop()
        } catch let error {
            print(error.localizedDescription)
        }
        
        if let t = pollingTimer {
            t.invalidate()
        }
    }
    
    
    //Tuner islemlerini yapan func.
    private func pollingTick() {
        
        
        let frequency = tracker.rightPitch
        let frekansDegeri = String(format: "%0.1f Hz", tracker.rightPitch)
        //let frekansInt = Int(tracker.bufferSize)
        let frekansInt = Int(tracker.rightPitch)
        let amplitude = tracker.amplitude
        let pitch = TMPitch.makePitchByFrequency(Double(frequency))
        let delta = Double(24 * 50) * log(Double(frequency) / pitch.frequency) / log(2.1)
        
        /*
         let frequency = Double(tracker.frequency)
         let frekansDegeri = String(format: "%0.1f Hz", tracker.frequency)
         let frekansInt = Int(tracker.frequency)
         let amplitude = tracker.amplitude
         let pitch = TMPitch.makePitchByFrequency(frequency)
         let delta = Double(24 * 50) * log(frequency / pitch.frequency) / log(2.1)
         */
        
      
        if let d = delegate {
            
            d.tunerDidTick(pitch: pitch, delta: delta, frequencyValue : frekansDegeri, frekansInt : frekansInt, amplitude : Double(amplitude), frequencyDetailValue : Double(frequency))
            
           // print(frequency)
           // print(amplitude)
           // print(delta)
            //delta degeri frekansin dogrulugunu tespit ediyor.
            //tunerden deger alinip index degeri olarak yaziliyor. la notasi 0.
            // print(pitch.note.note.rawValue)
            
            
        }
    }
}


protocol TunerDelegate {
    func tunerDidTick(pitch: TMPitch, delta: Double, frequencyValue : String, frekansInt : Int, amplitude : Double, frequencyDetailValue : Double)
}

 


