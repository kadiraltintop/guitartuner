//
//  TMNote.swift
//  Guitar Tuner
//
//  Created by Dev on 29.11.2020.
//

import Foundation


class TMNote: Equatable {
    enum Accidental: Int { case natural = 0, sharp, flat }
    enum Name: Int { case A = 0, B, C, D, E, F, G }
    
    static let all: [TMNote] = [
        TMNote(.C, .natural), TMNote(.C, .sharp),
        TMNote(.D, .natural),
        TMNote(.E, .flat), TMNote(.E, .natural),
        TMNote(.F, .natural), TMNote(.F, .sharp),
        TMNote(.G, .natural),
        TMNote(.A, .flat), TMNote(.A, .natural),
        TMNote(.B, .flat), TMNote(.B, .natural)
    ]
    
    var note: Name
    var accidental: Accidental
    
    var frequency: Double {
        let index = TMNote.all.firstIndex(of: self)! - TMNote.all.firstIndex(of: TMNote(.A, .natural))!
      
        
        return 440.0 * pow(2.0, Double(index) / 12.0)
    }
    
    init(_ note: Name, _ accidental: Accidental) {
        self.note = note
        self.accidental = accidental
    }
    
    static func ==(lhs: TMNote, rhs: TMNote) -> Bool {
        return lhs.note == rhs.note && lhs.accidental == rhs.accidental
    }
    
    static func !=(lhs: TMNote, rhs: TMNote) -> Bool {
        return !(lhs == rhs)
    }
}

