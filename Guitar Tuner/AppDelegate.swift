//
//  AppDelegate.swift
//  Guitar Tuner
//
//  Created by Dev on 28.11.2020.
//

import UIKit
import Firebase
import FirebaseMessaging
import Purchases
import GoogleMobileAds

@main
class AppDelegate: UIResponder, UIApplicationDelegate{


    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        GADMobileAds.sharedInstance().start(completionHandler: nil)
        
        let storyboard = UIStoryboard(name: "Splash", bundle: nil)

             let initialViewController = storyboard.instantiateViewController(withIdentifier: "Splash") as! SplashViewController

             self.window?.rootViewController = initialViewController
             self.window?.makeKeyAndVisible()
        
        
        connectPurchases()
        FirebaseApp.configure()
        
        UIApplication.shared.statusBarStyle = .lightContent
        
        // Override point for customization after application launch.
        return true
        

        
    }

    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}



extension AppDelegate : PurchasesDelegate{
    
    func connectPurchases(){
        Purchases.debugLogsEnabled = true
        Purchases.configure(withAPIKey: "uJFLZNCHtcgqCKcmHBVAlCUuBNYdGbLj")
        Purchases.shared.delegate = self
        
    }
    
    func purchases(_ purchases: Purchases, didReceiveUpdated purchaserInfo: Purchases.PurchaserInfo) {
        
        print("completed new IAP")
        //printte olsa bu fonsiyon calistirilmak zorunda.
        
    }
    
    
    func purchases(_ purchases: Purchases, shouldPurchasePromoProduct product: SKProduct, defermentBlock makeDeferredPurchase: @escaping RCDeferredPromotionalPurchaseBlock) {
        //Kullanici satin alma islemini sadece app icerisinde degil storedan da yapabiliyor. Eger storedan puchase edildiyse kontrolleri yapip premium aciliyor.
        // Save the deferment block and call it later...
        let defermentBlock = makeDeferredPurchase
        // ...or call it right away to proceed with the purchase
        defermentBlock { (transaction, info, error, cancelled) in
            if info?.entitlements.all["Premium"]?.isActive == true{
                // Unlock that great "pro" content
                UserDefaults.standard.set(true, forKey: "isPremium")
            }else{
                print("premium yok")
            }
        }
    }
}




